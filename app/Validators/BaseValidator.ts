export default class BaseValidator {
  public messages = {
    minLength: "{{ field }} must be at least {{ options.minLength }} characters long",
    maxLength: "{{ field }} must be less then {{ options.maxLength }} characters long",
    required: "{{ field }} is required",
    unique: "{{ field }} must be unique, and this value is already taken",
    "username.unique": "ID/Email is existed in system",
    "username.regex": "ID/Email not contains special character",
    "phone.unique": "Phone number already used for another account",
    confirmed: "Password and Password Confirmation are not the same",
  }
}
