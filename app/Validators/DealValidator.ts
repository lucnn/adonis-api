import { rules, schema } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import BaseValidator from "App/Validators/BaseValidator";

export default class DealValidator extends BaseValidator {
  constructor(protected ctx: HttpContextContract) {
    super();
  }

  public schema = schema.create({
    campaignId: schema.number([rules.required()]),
    isOpen: schema.number([rules.required(), rules.range(0, 1)]),
  });
}
