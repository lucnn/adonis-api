import { schema, rules } from '@ioc:Adonis/Core/Validator';
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BaseValidator from "App/Validators/BaseValidator";

export default class RegisterAdminValidator extends BaseValidator{
  constructor (protected ctx: HttpContextContract) {
    super()
  }

	/*
	 * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
	 *
	 * For example:
	 * 1. The username must be of data type string. But then also, it should
	 *    not contain special characters or numbers.
	 *    ```
	 *     schema.string({}, [ rules.alpha() ])
	 *    ```
	 *
	 * 2. The email must be of data type string, formatted as a valid
	 *    email. But also, not used by any other user.
	 *    ```
	 *     schema.string({}, [
	 *       rules.email(),
	 *       rules.unique({ table: 'users', column: 'email' }),
	 *     ])
	 *    ```
	 */
  public schema = schema.create({
    username: schema.string({ trim: true }, [
      rules.maxLength(50),
      rules.minLength(3),
      rules.unique({ table: 'admins', column: 'username' }),
      rules.regex(/^[a-zA-Z0-9-_.@]+$/),
      rules.notIn(['admin', 'super', 'moderator', 'public', 'dev', 'alpha', 'mail']) // ?
    ]),
    // email: schema.string({ trim: true }, [rules.unique({ table: 'users', column: 'email' })]),
    phone: schema.string({ trim: true }, [rules.unique({ table: 'admins', column: 'phone' })]),
    password: schema.string({}, [rules.minLength(8), rules.confirmed('passwordConfirm')])
  })

}
