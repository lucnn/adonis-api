import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import { schema } from "@ioc:Adonis/Core/Validator";
import BaseValidator from "App/Validators/BaseValidator";

export default class OrderValidator extends BaseValidator {
  constructor(protected ctx: HttpContextContract) {
    super();
  }

  public schema = schema.create({
    campaignId: schema.number([]),
    userAddressId: schema.number([]),
    quantity: schema.number([]),
    totalPrice: schema.number([]),
    shippingFee: schema.number([]),
    grandTotal: schema.number([]),
    paymentMethod: schema.number([]),
  });
}
