import { schema} from '@ioc:Adonis/Core/Validator';
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BaseValidator from "App/Validators/BaseValidator";

export default class RegisterValidator extends BaseValidator{
  constructor (protected ctx: HttpContextContract) {
    super()
  }

  public schema = schema.create({
    username: schema.string({ trim: true }),
    password: schema.string({}),
  })

}
