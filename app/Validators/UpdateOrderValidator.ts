import { rules, schema } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import BaseValidator from "App/Validators/BaseValidator";

export default class UpdateOrderValidator extends BaseValidator {
  constructor(protected ctx: HttpContextContract) {
    super();
  }

  public schema = schema.create({
    status: schema.number([rules.required()]),
    // shippingDate: schema.string({ trim: true }, []),
    // trackingNumber: schema.string({ trim: true }, []),
  });
}
