import { schema, rules } from '@ioc:Adonis/Core/Validator';
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BaseValidator from "App/Validators/BaseValidator";

export default class VerifyCodeValidator extends BaseValidator{
    constructor (protected ctx: HttpContextContract) {
        super()
    }

	/*
	 * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
	 *
	 * For example:
	 * 1. The username must be of data type string. But then also, it should
	 *    not contain special characters or numbers.
	 *    ```
	 *     schema.string({}, [ rules.alpha() ])
	 *    ```
	 *
	 * 2. The email must be of data type string, formatted as a valid
	 *    email. But also, not used by any other user.
	 *    ```
	 *     schema.string({}, [
	 *       rules.email(),
	 *       rules.unique({ table: 'users', column: 'email' }),
	 *     ])
	 *    ```
	 */
    public schema = schema.create({
        phone: schema.string({ trim: true }, [
			rules.maxLength(12),
			rules.minLength(9),
			// rules.regex(/(\(\+[0-9]{2}\)|0)([0-9]{9,10})/g),
			rules.notIn(['admin', 'super', 'moderator', 'public', 'dev', 'alpha', 'mail']) // ?
        ]),
		code: schema.string({ trim: true }, [
			rules.maxLength(6)
		]),
    })

}
