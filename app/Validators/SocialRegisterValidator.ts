import { schema, rules } from '@ioc:Adonis/Core/Validator';
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BaseValidator from "App/Validators/BaseValidator";

export default class SocialRegisterValidator extends BaseValidator{
    constructor (protected ctx: HttpContextContract) {
        super()
    }
    public schema = schema.create({
        phone: schema.string({ trim: true }),
        code: schema.string({}),
    })
}
