import { schema, rules } from '@ioc:Adonis/Core/Validator';
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BaseValidator from "App/Validators/BaseValidator";

export default class CampaignValidator extends BaseValidator{
    constructor (protected ctx: HttpContextContract) {
        super()
    }
    public schema = schema.create({
      name: schema.string({ trim: true }, [
        rules.maxLength(50)
      ]),
      brand: schema.string.optional({ trim: true }, [
        rules.maxLength(50)
      ]),
      content: schema.string.optional({ trim: true }, [
        rules.maxLength(300)
      ]),
    })

}
