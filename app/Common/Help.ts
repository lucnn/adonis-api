'use strict';
module.exports = class Help {
    formatMeta(meta) {
        if (meta) {
            if (meta.first_page_url) delete meta.first_page_url
            if (meta.last_page_url) delete meta.last_page_url
            if (meta.next_page_url) delete meta.next_page_url
            if (meta.previous_page_url) delete meta.previous_page_url
            return meta
        }
        return []
    }
    replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }
}
