import Logger from "@ioc:Adonis/Core/Logger";
import { BaseTask } from "adonis5-scheduler/build";
import Banner from "App/Models/Banner";
import Campaign from "App/Models/Campaign";
import Event from "App/Models/Event";
import Order from "App/Models/Order";
import CUtilsConfig from "Config/CUtilsConfig";

export default class MyTaskName extends BaseTask {
  public static get schedule() {
    // *    *    *    *    *    *
    // ┬    ┬    ┬    ┬    ┬    ┬
    // │    │    │    │    │    │
    // │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
    // │    │    │    │    └───── month (1 - 12)
    // │    │    │    └────────── day of month (1 - 31)
    // │    │    └─────────────── hour (0 - 23)
    // │    └──────────────────── minute (0 - 59)
    // └───────────────────────── second (0 - 59, OPTIONAL)
    return "0 * * * * *";
  }
  /**
   * Set enable use .lock file for block run retry task
   * Lock file save to `build/tmpTaskLock`
   */
  public static get useLock() {
    return false;
  }

  public async handle() {
    const timeCurrent = Math.floor(Date.now() / 1000);
    Logger.info("start check update status campaign");
    await Campaign.query()
      .where("deal_duration_start", "<", timeCurrent)
      .andWhere("status", ">", CUtilsConfig.status.warning)
      .update({ status: CUtilsConfig.status.warning });
    await Campaign.query()
      .where("deal_duration_start", "<", timeCurrent)
      .andWhere("deal_duration_end", ">", timeCurrent)
      .andWhere("status", "<>", CUtilsConfig.status.active)
      .update({ status: CUtilsConfig.status.active });
    await Campaign.query()
      .where("deal_duration_end", "<", timeCurrent)
      .andWhere("status", "<", CUtilsConfig.status.cancel)
      .update({ status: CUtilsConfig.status.cancel });
    await Campaign.query()
      .where("deal_duration_start", ">", timeCurrent)
      .andWhere("status", ">", CUtilsConfig.status.warning)
      .update({ status: CUtilsConfig.status.warning });
    Logger.info("end check update status campaign");

    // Logger.info('start check update status campaign')
    // await Campaign.query().where('deal_duration_start', '<', timeCurrent).andWhere('status', '>', CUtilsConfig.status.warning).andWhere({'mega_deal': CUtilsConfig.megaDeal.warning}).update({ 'status': CUtilsConfig.status.warning })
    // await Campaign.query().where('deal_duration_start', '<', timeCurrent).andWhere('deal_duration_end', '>', timeCurrent).andWhere('status', '<>', CUtilsConfig.status.active).andWhere({'mega_deal': CUtilsConfig.megaDeal.warning}).update({ 'status': CUtilsConfig.status.active })
    // await Campaign.query().where('deal_duration_end', '<', timeCurrent).andWhere('status', '<', CUtilsConfig.status.cancel).andWhere({'mega_deal': CUtilsConfig.megaDeal.warning}).update({ 'status': CUtilsConfig.status.cancel })
    // await Campaign.query().where('deal_duration_start', '>', timeCurrent).andWhere('status', '>', CUtilsConfig.status.warning).andWhere({'mega_deal': CUtilsConfig.megaDeal.warning}).update({ 'status': CUtilsConfig.status.warning })
    // Logger.info('end check update status campaign')
    //
    // Logger.info('start check update status campaign megadeal')
    // await Campaign.query().where('mega_deal_duration_start', '<', timeCurrent).andWhere('status', '>', CUtilsConfig.status.warning).andWhere({'mega_deal': CUtilsConfig.megaDeal.active}).update({ 'status': CUtilsConfig.status.warning })
    // await Campaign.query().where('mega_deal_duration_start', '<', timeCurrent).andWhere('mega_deal_duration_end', '>', timeCurrent).andWhere('status', '<>', CUtilsConfig.status.active).andWhere({'mega_deal': CUtilsConfig.megaDeal.active}).update({ 'status': CUtilsConfig.status.active })
    // await Campaign.query().where('mega_deal_duration_end', '<', timeCurrent).andWhere('status', '<', CUtilsConfig.status.cancel).andWhere({'mega_deal': CUtilsConfig.megaDeal.active}).update({ 'status': CUtilsConfig.status.cancel })
    // await Campaign.query().where('mega_deal_duration_start', '>', timeCurrent).andWhere('status', '>', CUtilsConfig.status.warning).andWhere({'mega_deal': CUtilsConfig.megaDeal.active}).update({ 'status': CUtilsConfig.status.warning })
    // Logger.info('end check update status campaign megadeal')

    Logger.info("start check update status banner");
    await Banner.query()
      .where("duration_start", "<", timeCurrent)
      .andWhere("status", ">", CUtilsConfig.status.warning)
      .update({ status: CUtilsConfig.status.warning });
    await Banner.query()
      .where("duration_start", "<", timeCurrent)
      .andWhere("duration_end", ">", timeCurrent)
      .andWhere("status", "<>", CUtilsConfig.status.active)
      .update({ status: CUtilsConfig.status.active });
    await Banner.query()
      .where("duration_end", "<", timeCurrent)
      .andWhere("status", "<", CUtilsConfig.status.cancel)
      .update({ status: CUtilsConfig.status.cancel });
    await Banner.query()
      .where("duration_start", ">", timeCurrent)
      .andWhere("status", ">", CUtilsConfig.status.warning)
      .update({ status: CUtilsConfig.status.warning });
    Logger.info("end check update status banner");

    Logger.info("start check update status event");
    await Event.query()
      .where("duration_start", "<", timeCurrent)
      .andWhere("status", ">", CUtilsConfig.status.warning)
      .update({ status: CUtilsConfig.status.warning });
    await Event.query()
      .where("duration_start", "<", timeCurrent)
      .andWhere("duration_end", ">", timeCurrent)
      .andWhere("status", "<>", CUtilsConfig.status.active)
      .update({ status: CUtilsConfig.status.active });
    await Event.query()
      .where("duration_end", "<", timeCurrent)
      .andWhere("status", "<", CUtilsConfig.status.cancel)
      .update({ status: CUtilsConfig.status.cancel });
    await Event.query()
      .where("duration_start", ">", timeCurrent)
      .andWhere("status", ">", CUtilsConfig.status.warning)
      .update({ status: CUtilsConfig.status.warning });
    Logger.info("end check update status event");

    Logger.info("start check update status Order");
    await Order.query()
      .innerJoin("deals", "orders.deal_id", "=", "deals.id")
      .select("orders.*")
      .where("orders.status", "=", CUtilsConfig.orderStatus.pending)
      .where("deals.status", "=", CUtilsConfig.dealStatus.active)
      .whereRaw("deals.expire_at < NOW()")
      .update({ "orders.status": CUtilsConfig.orderStatus.cancelled });
    Logger.info("end check update status Order");
  }
}
