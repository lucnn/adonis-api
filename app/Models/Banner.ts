import { DateTime } from 'luxon'
import Event from "App/Models/Event";
import { BaseModel, column, belongsTo, BelongsTo } from '@ioc:Adonis/Lucid/Orm'

/**
* @swagger
* definitions:
*   Banner:
*     type: object
*     properties:
*       id:
*         type: number
*       name:
*         type: string
*       path:
*         type: string
*       order:
*         type: number
*       type:
*         type: number
*       position:
*         type: number
*       status:
*         type: number
*       durationStart:
*         type: number
*       durationEnd:
*         type: number
*       eventId:
*         type: number
*       createdAt:
*         type: string
*       updatedAt:
*         type: string
*/


export default class Banner extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string;

  @column()
  public path: string;

  @column()
  public order: number;

  @column()
  public type: number;

  @column()
  public position: number;

  @column()
  public status: number;

  @column()
  public durationStart: number;

  @column()
  public durationEnd: number;

  @column()
  public eventId: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  // Relationship
  @belongsTo(() => Event)
  public event: BelongsTo<typeof Event>;
}
