import { DateTime } from "luxon";
import { BaseModel, belongsTo, BelongsTo, column } from "@ioc:Adonis/Lucid/Orm";
import User from "./User";
import Campaign from "./Campaign";

export default class Deal extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column()
  public campaignId: number;

  @column()
  public makerId: number;

  @column()
  public orderMakerId: number | null;

  @column()
  public orderMatcherId: number | null;

  @column()
  public isOpen: number;

  @column()
  public status: number;

  @column.dateTime()
  public expireAt: DateTime;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @belongsTo(() => User, {
    foreignKey: "makerId",
  })
  public maker: BelongsTo<typeof User>;

  @belongsTo(() => Campaign)
  public campaign: BelongsTo<typeof Campaign>;
}
