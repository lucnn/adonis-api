'use strict'

/**
 *  @swagger
 *  definitions:
 *    Meta:
 *      type: object
 *      properties:
 *        total:
 *          type: number
 *        perPage:
 *          type: number
 *        currentPage:
 *          type: number
 *        lastPage:
 *          type: number
 *        firstPage:
 *          type: number
 */


/**
 *  @swagger
 *  definitions:
 *    Pagination:
 *      type: object
 *      properties:
 *        id:
 *          type: uint
 *        perPage:
 *          type: uint
 *        page:
 *          type: uint
 */

/**
 * @swagger
 * definitions:
 *   SuccessResponse:
 *     type: object
 *     properties:
 *       success:
 *         type: boolean
 *       message:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   ErrorResponse:
 *     type: object
 *     properties:
 *       success:
 *         type: boolean
 *         example: false
 *       message:
 *         type: string
 *       statusCode:
 *          type: integer
 *       errors:
 *         type: object
 */

/**
 * @swagger
 * definitions:
 *   AuthLogin:
 *     type: object
 *     properties:
 *       username:
 *         type: string
 *         example: "lucnn"
 *         required: true
 *       password:
 *         type: string
 *         example: "123456"
 *         required: true
 */

/**
 * @swagger
 * definitions:
 *   ResCode:
 *     type: object
 *     properties:
 *       expiredAt:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   PathFile:
 *     type: object
 *     properties:
 *       path:
 *         type: string
 */
