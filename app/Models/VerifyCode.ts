import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class VerifyCode extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public type: number;

  @column()
  public expiredAt: number;

  @column()
  public lockCount: number;

  @column()
  public lockTime: number;

  @column()
  public status: number;

  @column()
  public payload: string;

  @column()
  public token_very: string;

  @column()
  public code: string;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
