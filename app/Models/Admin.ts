import { DateTime } from "luxon";
import Hash from "@ioc:Adonis/Core/Hash";
import {
  column,
  beforeSave,
  BaseModel,
} from "@ioc:Adonis/Lucid/Orm";

/**
 *  @swagger
 *  definitions:
 *    AdminInfo:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        name:
 *          type: string
 *        email:
 *          type: string
 *        avatar:
 *          type: string
 *        username:
 *          type: string
 *        birthday:
 *          type: string
 *        phone:
 *          type: string
 *        veryPhone:
 *          description: 0 - chưa verify, 1 - đã verify
 *          type: integer
 *        token:
 *          type: string
 *        tokenExpiresAt:
 *          description: thời hạn token
 *          type: string
 */

/**
 *  @swagger
 *  definitions:
 *    AdminRegister:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *        email:
 *          type: string
 *        avatar:
 *          type: string
 *        username:
 *          type: string
 *        birthday:
 *          example: "2021-10-10"
 *          type: string
 *        phone:
 *          type: string
 *        password:
 *          type: string
 *        passwordConfirm:
 *          type: string
 */
export default class Admin extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
// @ts-ignore
  @column({ unique: true })
  public username: string;
// @ts-ignore
  @column({ unique: true })
  public phone: string;

  @column()
  public email: string;

  @column()
  public name: string;

  @column()
  public avatar: string;

  @column({ serializeAs: null })
  public password: string;

  @column()
  public rememberMeToken?: string;

  @column()
  public type: number;

  @column()
  public very_phone: number;

  @column.dateTime()
  public birthday: DateTime;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @beforeSave()
  public static async hashPassword(user: Admin) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password);
    }
  }
}
