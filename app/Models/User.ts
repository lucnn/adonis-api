import Hash from "@ioc:Adonis/Core/Hash";
import { BaseModel, beforeSave, column } from "@ioc:Adonis/Lucid/Orm";
import { DateTime } from "luxon";

/**
 *  @swagger
 *  definitions:
 *    UserInfo:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        name:
 *          type: string
 *        email:
 *          type: string
 *        username:
 *          type: string
 *        avatar:
 *          type: string
 *        birthday:
 *          type: string
 *        phone:
 *          type: string
 *        point:
 *          type: number
 *        veryPhone:
 *          description: 0 - chưa verify, 1 - đã verify
 *          type: integer
 *        token:
 *          type: string
 *        tokenExpiresAt:
 *          description: thời hạn token
 *          type: string
 */

/**
 *  @swagger
 *  definitions:
 *    UserRegister:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *        email:
 *          type: string
 *        username:
 *          type: string
 *        avatar:
 *          type: string
 *        birthday:
 *          example: "2021-10-10"
 *          type: string
 *        phone:
 *          type: string
 *        password:
 *          type: string
 *        passwordConfirm:
 *          type: string
 *        code:
 *          type: string
 */

/**
 *  @swagger
 *  definitions:
 *    ForgotPassword:
 *      type: object
 *      properties:
 *        phone:
 *          type: string
 *        password:
 *          type: string
 *        passwordConfirm:
 *          type: string
 *        tokenVery:
 *          type: string
 */

/**
 *  @swagger
 *  definitions:
 *    LoginSocial:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *        email:
 *          type: string
 *        avatar:
 *          type: string
 *        birthday:
 *          example: "2021-10-10"
 *          type: string
 *        phone:
 *          type: string
 *        code:
 *          type: string
 */
export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  // @ts-ignore
  @column({ unique: true })
  public username: string;
  // @ts-ignore
  @column({ unique: true })
  public phone: string;

  @column()
  public email: string;

  @column()
  public name: string;

  @column()
  public avatar: string;

  @column({ serializeAs: null })
  public password: string;

  @column()
  public rememberMeToken?: string;

  @column()
  public type: number;

  @column()
  public very_phone: number;

  @column.dateTime()
  public birthday: DateTime;

  @column()
  public point: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @beforeSave()
  public static async hashPassword(user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password);
    }
  }
}
