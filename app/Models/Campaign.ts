import { DateTime } from 'luxon'
import Order from "App/Models/Order";
import {BaseModel, BelongsTo, belongsTo, column, hasMany, HasMany} from '@ioc:Adonis/Lucid/Orm'
import Category from "App/Models/Category";

/**
 * @swagger
 * definitions:
 *   Campaign:
 *     type: object
 *     properties:
 *              id:
 *                type: number
 *              name:
 *                description: name
 *                type: string
 *              brand:
 *                description: brand
 *                type: string
 *              thumbnail:
 *                description: thumbnail
 *                type: string
 *              content:
 *                description: content
 *                type: string
 *              images:
 *                description: images
 *                type: object
 *              options:
 *                description: options
 *                type: object
 *              benefitsTag:
 *                description: benefits tag
 *                type: object
 *              status:
 *                description: status
 *                type: integer
 *              amountOfSales:
 *                description: amountOfSales
 *                type: integer
 *              participationRate:
 *                description: participationRate
 *                type: integer
 *              accomplishmentRate:
 *                description: accomplishmentRate
 *                type: integer
 *              userId:
 *                description: user id
 *                type: integer
 *              adminId:
 *                description: admin id
 *                type: integer
 *              viewCount:
 *                description: view count
 *                type: integer
 *              categoryId:
 *                description: category id
 *                type: integer
 *              dealDurationStart:
 *                description: deal duration start timestamp
 *                type: integer
 *              dealDurationEnd:
 *                description: deal duration end timestamp
 *                type: integer
 *              originalPrice:
 *                description: original price
 *                type: number
 *              discountPrice:
 *                description: discount price
 *                type: number
 *              dealPrice:
 *                description: deal price
 *                type: number
 *              megaDeal:
 *                description: mega deal
 *                type: integer
 *              extraDiscountRate:
 *                description: extra discount rate
 *                type: number
 *              refundPoints:
 *                description: refund_points
 *                type: number
 *              targetNumberPeople:
 *                description: target number people
 *                type: number
 *              megaDealDurationStart:
 *                description: mega deal duration start timestamp
 *                type: integer
 *              megaDealDurationEnd:
 *                description: mega deal duration end timestamp
 *                type: integer
 *              freeShipping:
 *                description: freeShipping
 *                type: number
 *                required: false
 *              shippingFee:
 *                description: shippingFee
 *                type: number
 *                required: false
 *              favorite:
 *                description: favorite
 *                type: number
 */

/**
 * @swagger
 * definitions:
 *   CampaignForm:
 *     type: object
 *     properties:
 *              name:
 *                description: name
 *                type: string
 *                required: true
 *              brand:
 *                description: brand
 *                type: string
 *                required: false
 *              thumbnail:
 *                description: thumbnail
 *                type: string
 *                required: false
 *              content:
 *                description: content
 *                type: string
 *              images:
 *                description: images
 *                type: object
 *              options:
 *                description: options
 *                type: object
 *              benefitsTag:
 *                description: benefits tag
 *                type: object
 *              categoryId:
 *                description: category id
 *                type: integer
 *                required: false
 *              dealDurationStart:
 *                description: deal duration start timestamp
 *                type: integer
 *                required: false
 *              dealDurationEnd:
 *                description: deal duration end timestamp
 *                type: integer
 *                required: false
 *              originalPrice:
 *                description: original price
 *                type: number
 *                required: false
 *              discountPrice:
 *                description: discount price
 *                type: number
 *                required: false
 *              dealPrice:
 *                description: deal price
 *                type: number
 *                required: false
 *              megaDeal:
 *                description: mega deal
 *                type: integer
 *                required: false
 *              extraDiscountRate:
 *                description: extra discount rate
 *                type: number
 *                required: false
 *              refundPoints:
 *                description: refund_points
 *                type: number
 *                required: false
 *              targetNumberPeople:
 *                description: target number people
 *                type: number
 *                required: false
 *              megaDealDurationStart:
 *                description: mega deal duration start timestamp
 *                type: integer
 *                required: false
 *              megaDealDurationEnd:
 *                description: mega deal duration end timestamp
 *                type: integer
 *                required: false
 *              freeShipping:
 *                description: freeShipping
 *                type: number
 *                required: false
 *              shippingFee:
 *                description: shippingFee
 *                type: number
 *                required: false
 */

/**
 * @swagger
 * definitions:
 *   CampaignFavorite:
 *     type: object
 *     properties:
 *              status:
 *                description: 0 - cancel favorite, 1 - set favorite
 *                type: integer
 *                required: true
 *              campaignId:
 *                description: campaign_id
 *                type: integer
 *                required: true
 */

export default class Campaign extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string;

  @column()
  public brand: string;

  @column()
  public thumbnail: number;

  @column()
  public content: string;

  @column()
  public images: string;

  @column()
  public options: string;

  @column()
  public benefitsTag: string;

  @column()
  public status: number;

  @column()
  public userId: number;

  @column()
  public adminId: number;

  @column()
  public viewCount: number;

  @column()
  public categoryId: number;

  @column()
  public participationRate: number;

  @column()
  public accomplishmentRate: number;

  @column()
  public freeShipping: number;

  @column()
  public shippingFee: number;

  @column()
  public dealDurationStart: number;

  @column()
  public dealDurationEnd: number;

  @column()
  public originalPrice: number;

  @column()
  public discountPrice: number;

  @column()
  public dealPrice: number;

  @column()
  public amountOfSales: number;

  @column()
  public megaDeal: number;

  @column()
  public extraDiscountRate: number;

  @column()
  public refundPoints: number;

  @column()
  public targetNumberPeople: number;

  @column()
  public megaDealDurationStart: number;

  @column()
  public megaDealDurationEnd: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasMany(() => Order)
  public order: HasMany<typeof Order>;

  // Relationship
  @belongsTo(() => Category)
  public category: BelongsTo<typeof Category>;
}
