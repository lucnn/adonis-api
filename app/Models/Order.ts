import {
  BaseModel,
  beforeFetch,
  beforeFind,
  belongsTo,
  BelongsTo,
  column,
  ModelQueryBuilderContract,
} from "@ioc:Adonis/Lucid/Orm";
import Campaign from "App/Models/Campaign";
import UserAddress from "App/Models/UserAddress";
import { DateTime } from "luxon";
import Deal from "./Deal";
import User from "./User";

/**
 * @swagger
 * definitions:
 *   Order:
 *     type: object
 *     properties:
 *       id:
 *         type: number
 *       user_id:
 *         type: number
 *       campaign_id:
 *         type: number
 *       user_address_id:
 *         type: number
 *       options:
 *         type: string
 *       status:
 *         type: number
 *       quantity:
 *         type: number
 *       total_price:
 *         type: number
 *       shipping_fee:
 *         type: number
 *       grand_total:
 *         type: number
 *       payment_method:
 *         type: number
 *       createdAt:
 *         type: string
 *       updatedAt:
 *         type: string
 */

/**
 * @swagger
 * definitions:
 *   OrderForm:
 *     type: object
 *     properties:
 *              campaignId:
 *                description: campaignId order
 *                type: number
 *                required: true
 *              userAddressId:
 *                description: userAddressId order
 *                type: number
 *                required: true
 *              options:
 *                description: options order
 *                type: object
 *                required: true
 *              quantity:
 *                description: quantity order
 *                type: number
 *                required: false
 *              totalPrice:
 *                description: totalPrice order
 *                type: integer
 *                required: false
 *              shippingFee:
 *                description: shippingFee order
 *                type: integer
 *                required: false
 *              grandTotal:
 *                description: grandTotal order
 *                type: integer
 *                required: false
 *              paymentMethod:
 *                description: paymentMethod order
 *                type: integer
 *                required: false
 */

export default class Order extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column()
  public userId: number;

  @column()
  public campaignId: number;

  @column()
  public dealId: number | null;

  @column()
  public userAddressId: number;

  @column()
  public options: string;

  @column()
  public status: number;

  @column()
  public quantity: number;

  @column()
  public totalPrice: number;

  @column()
  public shippingFee: number;

  @column()
  public grandTotal: number;

  @column()
  public paymentMethod: number;

  @column()
  public pointDiscount: number;

  @column()
  public shippingDate: string;

  @column()
  public trackingNumber: string;

  @column.dateTime()
  public expireAt: DateTime;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @column.dateTime()
  public deletedAt?: DateTime;

  // Relationship
  @belongsTo(() => Campaign)
  public campaign: BelongsTo<typeof Campaign>;

  @belongsTo(() => Deal)
  public deal: BelongsTo<typeof Deal>;

  @belongsTo(() => UserAddress)
  public userAddress: BelongsTo<typeof UserAddress>;

  @belongsTo(() => User)
  public user: BelongsTo<typeof User>;

  @beforeFind()
  public static ignoreDeleted(query: ModelQueryBuilderContract<typeof Order>) {
    query.whereNull("deleted_at");
  }

  @beforeFetch()
  public static filterDeleted(query: ModelQueryBuilderContract<typeof Order>) {
    query.whereNull("deleted_at");
  }
}
