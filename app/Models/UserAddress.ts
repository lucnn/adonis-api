import { DateTime } from 'luxon'
import Order from "App/Models/Order";
import { BaseModel, column, hasMany, HasMany } from '@ioc:Adonis/Lucid/Orm'

/**
* @swagger
* definitions:
*   UserAddress:
*     type: object
*     properties:
*       id:
*         type: number
*       userId:
*         type: number
*       contactName:
*         type: string
*       phone:
*         type: string
*       city:
*         type: string
*       district:
*         type: string
*       wards:
*         type: string
*       address:
*         type: string
*       setDefault:
*         type: number
*       createdAt:
*         type: string
*       updatedAt:
*         type: string
*/


export default class UserAddress extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public userId: number;

  @column()
  public contactName: string;

  @column()
  public phone: string;

  @column()
  public city: number;

  @column()
  public district: number;

  @column()
  public wards: number;

  @column()
  public address: string;

  @column()
  public setDefault: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasMany(() => Order)
  public order: HasMany<typeof Order>;
}
