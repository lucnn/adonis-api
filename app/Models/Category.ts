import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

/**
* @swagger
* definitions:
*   Category:
*     type: object
*     properties:
*       id:
*         type: number
*       name:
*         type: string
*       description:
*         type: string
*       icon:
*         type: string
*       order:
*         type: number
*       type:
*         type: number
*       status:
*         type: number
*       createdAt:
*         type: string
*       updatedAt:
*         type: string
*/

export default class Category extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string;

  @column()
  public description: string;

  @column()
  public icon: string;

  @column()
  public parentId: number;

  @column()
  public order: number;

  @column()
  public type: number;

  @column()
  public status: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
