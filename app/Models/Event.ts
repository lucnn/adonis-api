import { DateTime } from 'luxon'
import Banner from "App/Models/Banner";
import { BaseModel, column, hasMany, HasMany  } from '@ioc:Adonis/Lucid/Orm'

/**
 * @swagger
 * definitions:
 *   Event:
 *     type: object
 *     properties:
 *              id:
 *                type: number
 *              title:
 *                description: title event
 *                type: string
 *                required: true
 *              image:
 *                description: image event
 *                type: string
 *                required: true
 *              content:
 *                description: content event
 *                type: string
 *                required: true
 *              status:
 *                description: status event
 *                type: integer
 *                required: false
 *              durationStart:
 *                description: duration start event
 *                type: integer
 *                required: false
 *              durationEnd:
 *                description: duration end event
 *                type: integer
 *                required: false
 */

/**
 * @swagger
 * definitions:
 *   EventForm:
 *     type: object
 *     properties:
 *              title:
 *                description: title event
 *                type: string
 *                required: true
 *              image:
 *                description: image event
 *                type: string
 *                required: true
 *              content:
 *                description: content event
 *                type: string
 *                required: true
 *              durationStart:
 *                description: duration start event
 *                type: integer
 *                required: false
 *              durationEnd:
 *                description: duration end event
 *                type: integer
 *                required: false
 */

export default class Event extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public title: string;

  @column()
  public image: string;

  @column()
  public content: string;

  @column()
  public status: number;

  @column()
  public durationStart: number;

  @column()
  public durationEnd: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasMany(() => Banner)
  public banners: HasMany<typeof Banner>;
}
