import {
    HttpContextContract
} from "@ioc:Adonis/Core/HttpContext";
import UserAddress from "App/Models/UserAddress";
import Logger from "@ioc:Adonis/Core/Logger";
const Help = require("App/Common/Help");
let helper = new Help()

export default class UserAddressController {

    /**
     * @swagger
     * /api/user-address:
     *   get:
     *     tags:
     *       - User Address
     *     summary: List address
     *     security:
     *       - bearerAuth: []
     *     parameters:
     *       - name: page
     *         description: page
     *         in: query
     *         required: false
     *       - name: limit
     *         description: limit
     *         in: query
     *         required: false
     *     responses:
     *       200:
     *         description: Kết quả khi thành công
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/SuccessResponse'
     *              properties:
     *                data:
     *                  schema:
     *                  $ref: '#/definitions/UserAddress'
     *                meta:
     *                  schema:
     *                  $ref: '#/definitions/Meta'
     *       400:
     *         description: Kết quả khi lỗi
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/ErrorResponse'
     */
    public async index({ request, response, auth }: HttpContextContract) {
        try {
            const user = await auth.authenticate();
            let page = request.input('page', 1)
            let limit = request.input('limit', 20)
            let params = {}
            params["user_id"] = user.id
            const userAddress = await UserAddress.query().where(params).orderBy('created_at', 'desc').paginate(page, limit);
            let userAddressData = userAddress.toJSON().data
            let userAddressMeta = userAddress.toJSON().meta
            userAddressMeta = helper.formatMeta(userAddressMeta)
            const userAddressJson = userAddressData.map((userAddressData) => userAddressData.serialize())
            Logger.info({
                userAddress: userAddressData.length
            }, "Get list address successfully");
            let responseData = {
                success: true,
                message: 'success',
                data: userAddressJson,
                meta: userAddressMeta
            }
            return response.json(responseData)
        } catch (error) {
            Logger.error({
                err: new Error(error)
            }, "Get list address failed");
            return response.status(400).send({
                success: false,
                statusCode: 400,
                message: error.message,
                errors: error
            })
        }
    }

    /**
     * @swagger
     * /api/user-address/{id}:
     *   get:
     *     tags:
     *       - User Address
     *     summary: Detail User Address
     *     security:
     *       - bearerAuth: []
     *     parameters:
     *       - name: id
     *         description: id Address
     *         in: path
     *         required: true
     *     responses:
     *       200:
     *         description: Kết quả khi thành công
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/SuccessResponse'
     *              properties:
     *                data:
     *                  schema:
     *                  $ref: '#/definitions/UserAddress'
     *       400:
     *         description: Kết quả khi lỗi
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/ErrorResponse'
     */
    public async show({ request, response, auth }: HttpContextContract) {
        try {
            const user = await auth.authenticate();
            let id = request.param('id')
            const userAddress = await UserAddress.query().where({id: id, user_id: user.id}).first();
            if (!userAddress) {
                return response.status(400).send({
                    success: false,
                    statusCode: 400,
                    message: "Address not found",
                    messageKey: "ADDRESS_NOT_FOUND",
                    data: null
                })
            }

            let responseData = {
                success: true,
                message: 'success',
                data: userAddress ? userAddress?.toJSON() : null
            }
            Logger.info({ address: id }, "Get address successfully");
            return response.json(responseData)
        } catch (error) {
            Logger.error({ err: new Error(error) }, "Get address failed");
            return response.status(400).send({
                success: false,
                statusCode: 400,
                message: error.message,
                errors: error
            })
        }
    }

    /**
     * @swagger
     * /api/user-address/{id}:
     *   put:
     *     tags:
     *       - User Address
     *     summary: Update User Address
     *     security:
     *       - bearerAuth: []
     *     parameters:
     *       - name: id
     *         description: id address
     *         in: path
     *         required: true
     *     requestBody:
     *      required: true
     *      content:
     *        application/json:
     *          schema:
     *            properties:
     *              contactName:
     *                description: contact name address
     *                type: string
     *                required: false
     *              phone:
     *                description: phone address
     *                type: string
     *                required: false
     *              city:
     *                description: city address
     *                type: string
     *                required: false
     *              district:
     *                description: district address
     *                type: string
     *                required: false
     *              wards:
     *                description: wards address
     *                type: string
     *                required: false
     *              address:
     *                description: address
     *                type: string
     *                required: false
     *              setDefault :
     *                description: set default
     *                type: number
     *                required: false
     *     responses:
     *       200:
     *         description: Kết quả khi thành công
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/SuccessResponse'
     *              properties:
     *                data:
     *                  schema:
     *                  $ref: '#/definitions/UserAddress'
     *       400:
     *         description: Kết quả khi lỗi
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/ErrorResponse'
     */
    public async update({ request, response, auth }: HttpContextContract) {
        try {
            const user = await auth.authenticate();
            // const userAddress = await UserAddress.find(request.param('id'));
            let userAddress = await UserAddress.query().where({id: request.param('id'), user_id: user.id}).first();
            let responseData = {
                success: true,
                message: 'success',
                messageKey: '',
                data: []
            }
            if (userAddress) {
                userAddress.contactName = request.input("contactName");
                userAddress.phone = request.input("phone");
                userAddress.city = request.input("city");
                userAddress.district = request.input("district");
                userAddress.wards = request.input("wards");
                userAddress.setDefault = request.input("setDefault");
                userAddress.address = request.input("address");
                if (await userAddress.save()) {
                    // @ts-ignore
                    responseData.data = userAddress.serialize()
                    return response.json(responseData)
                } else {
                    responseData.success = false
                    responseData.message = "Update address fail"
                    responseData.messageKey = "UPDATE_ADDRESS_FAIL"
                    return response.status(422).send(responseData)
                }
            } else {
                return response.status(422).send({
                    success:  false,
                    statusCode: 422,
                    message: "Address not found",
                    messageKey: "ADDRESS_NOT_FOUND",
                    data: null
                })
            }
        } catch (error) {
            Logger.error({
                err: new Error(error)
            }, "Update address failed");
            return response.status(400).send({
                success: false,
                statusCode: 400,
                message: error.message,
                errors: error
            })
        }
    }

    /**
     * @swagger
     * /api/user-address:
     *   post:
     *     tags:
     *       - User Address
     *     summary: Create User Address
     *     security:
     *       - bearerAuth: []
     *     requestBody:
     *      required: true
     *      content:
     *        application/json:
     *          schema:
     *            properties:
     *              contactName:
     *                description: contact name
     *                type: string
     *                required: true
     *              phone:
     *                description: phone
     *                type: string
     *                required: true
     *              city :
     *                description: city
     *                type: string
     *                required: false
     *              district:
     *                description: district
     *                type: string
     *                required: false
     *              wards:
     *                description: wards
     *                type: string
     *                required: false
     *              address:
     *                description: address
     *                type: string
     *                required: false
     *              setDefault:
     *                description: setDefault
     *                type: integer
     *                required: false
     *     responses:
     *       200:
     *         description: Kết quả khi thành công
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/SuccessResponse'
     *              properties:
     *                data:
     *                  schema:
     *                  $ref: '#/definitions/UserAddress'
     *       400:
     *         description: Kết quả khi lỗi
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/ErrorResponse'
     */
    public async store({ response, request, auth }: HttpContextContract) {
        try {
            const user = await auth.authenticate();
            const userAddress = new UserAddress();
            userAddress.contactName = request.input("contactName");
            userAddress.phone = request.input("phone");
            userAddress.city = request.input("city");
            userAddress.district = request.input("district");
            userAddress.wards = request.input("wards");
            userAddress.setDefault = request.input("setDefault");
            userAddress.address = request.input("address");
            userAddress.userId = user.id;
            await userAddress.save();
            let userAddressJson = userAddress.serialize()
            Logger.info({
                userAddress: userAddress.id
            }, "Create address successfully");
            let responseData = {
                success: true,
                message: 'success',
                data: userAddressJson
            }
            return response.json(responseData)
        } catch (error) {
            Logger.error({
                err: new Error(error)
            }, "Create address failed");
            return response.status(400).send({
                success: false,
                statusCode: 400,
                message: error.message,
                errors: error
            })
        }
    }

    /**
     * @swagger
     * /api/user-address/{id}:
     *   delete:
     *     tags:
     *       - User Address
     *     summary: Delete User Address
     *     security:
     *       - bearerAuth: []
     *     parameters:
     *       - name: id
     *         description: id address
     *         in: path
     *         required: true
     *     responses:
     *       200:
     *         description: Kết quả khi thành công
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/SuccessResponse'
     *       400:
     *         description: Kết quả khi lỗi
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/ErrorResponse'
     */
    public async destroy({ response, request, auth }: HttpContextContract) {
        try {
            const user = await auth.authenticate();
            let userAddress = await UserAddress.query()
                .where("id", request.param('id'))
                .where("user_id", user.id)
                .delete();
            if (!userAddress[0]) {
                return response.status(400).send({
                    success: false,
                    statusCode: 400,
                    message: "Address not found",
                    messageKey: "ADDRESS_NOT_FOUND",
                    data: null
                })
            }
            return response.status(200).send({
                success: true,
                message: "Success",
                data: null
            })
        } catch (error) {
            Logger.error({
                err: new Error(error)
            }, "Delete address failed");
            return response.status(400).send({
                success: false,
                statusCode: 400,
                message: error.message,
                errors: error
            })
        }
    }

        /**
     * @swagger
     * /api/update-address-default/{id}:
     *   put:
     *     tags:
     *       - User Address
     *     summary: Update User Address Default
     *     security:
     *       - bearerAuth: []
     *     parameters:
     *       - name: id
     *         description: id address
     *         in: path
     *         required: true
     *     responses:
     *       200:
     *         description: Kết quả khi thành công
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/SuccessResponse'
     *       400:
     *         description: Kết quả khi lỗi
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/ErrorResponse'
     */
        public async updateAddressDefault({ response, request, auth }: HttpContextContract) {
            try {
                let id = request.param('id')
                const user = await auth.authenticate();
                let userAddress = await UserAddress.query()
                    .where("id", id)
                    .where("user_id", user.id)
                    .first();
                if (!userAddress) {
                    return response.status(400).send({
                        success: false,
                        statusCode: 400,
                        message: "Address not found",
                        messageKey: "ADDRESS_NOT_FOUND",
                        data: null
                    })
                }
                await UserAddress.query().where({ 'user_id': user.id }).update({ 'set_default': 0 })
                userAddress.setDefault = 1
                await userAddress.save()
                return response.status(200).send({
                    success: true,
                    message: "Success",
                    data: null
                })
            } catch (error) {
                Logger.error({
                    err: new Error(error)
                }, "Delete address failed");
                return response.status(400).send({
                    success: false,
                    statusCode: 400,
                    message: error.message,
                    errors: error
                })
            }
        }
}
