import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import User from "App/Models/User";
import Logger from "@ioc:Adonis/Core/Logger";
import RegisterValidator from 'App/Validators/RegisterValidator';
import LoginValidator from 'App/Validators/LoginValidator';
import ForgotPasswordValidator from 'App/Validators/ForgotPasswordValidator';
import VerifyCode from "App/Models/VerifyCode";
import CUtilsConfig from "Config/CUtilsConfig";
const isEmpty = require('lodash.isempty')

export default class AuthController {
  /**
   * @swagger
   * /api/login:
   *   post:
   *     tags:
   *       - Auth
   *     summary: Login API
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/definitions/AuthLogin'
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/UserInfo'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async login({ request, auth, response }: HttpContextContract) {
    try {
      await request.validate(LoginValidator)
      const params = request.body();
      
      const tokenInfo = await auth.use("api").attempt(params.username, params.password, {
        expiresIn: CUtilsConfig.token.expiresIn,
      });
      Logger.info({ user: auth.user!.id }, "User login successfully");
      const userInfo = await User.find(auth.user!.id);
      if (userInfo?.type != CUtilsConfig.userType.default) {
        return response.status(403).send({
          success: false,
          statusCode: 403,
          message: 'Access was denied',
        })
      }
      let responseData = {
        success: true,
        message: 'success',
          data: {...userInfo?.toJSON(), token: tokenInfo.token, tokenExpiresAt: tokenInfo.expiresAt!.toString()}
      }
      return response.json(responseData)
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api/register:
   *   post:
   *     tags:
   *       - Auth
   *     summary: Register API
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/definitions/UserRegister'
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/UserInfo'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async register({ request, auth, response }: HttpContextContract) {
    try {
      await request.validate(RegisterValidator)

      const params = request.body();
      let checkCode = await VerifyCode.query().where({'payload': params.phone, 'type': 0, 'status': 1, 'code': params.code}).first()
      if(isEmpty(checkCode)){
        return response.status(400).send({
          success: false,
          message: 'Authentication code is expired',
          statusCode: 400
        })
      }
      /**
       * Create a new user
       */
      params.very_phone = 1
      delete params.passwordConfirm
      delete params.code
      const user = await User.create(params);
      Logger.info({ user: user.id }, "User register successfully");

      const tokenInfo = await auth.use("api").attempt(params.username, params.password, {
        expiresIn: CUtilsConfig.token.expiresIn,
      });

      Logger.info({ user: user.id }, "User login successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: {...user?.toJSON(), token: tokenInfo.token, tokenExpiresAt: tokenInfo.expiresAt!.toString()}
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "User register failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api/forgot-password:
   *   post:
   *     tags:
   *       - Auth
   *     summary: Forgot Password API
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/definitions/ForgotPassword'
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async forgotPassword({ request, response }: HttpContextContract) {
    try {
      await request.validate(ForgotPasswordValidator)
      let phone = request.input("phone")
      let tokenVery = request.input("tokenVery")
      let password = request.input("password")
      let result = {
        success: false,
        code: 400,
        message: "The account does not exist. Please enter a valid user information",
        messageKey: "USER_NOT_EXIST",
        data: null
      }
      let verifyData = await VerifyCode.query().where({
        'payload': phone,
        'token_very': tokenVery
      }).first();
      if (isEmpty(verifyData) || verifyData!.expiredAt < Math.floor(Date.now() / 1000)) {
        return response.status(result.code).send(result)
      }
      let userInfo = await User.query().where({
        'phone': phone,
      }).first();
      userInfo!.password = password

      if (await userInfo!.save()) {
        result.success = true
        result.code = 200,
        result.message = "Success"
        // @ts-ignore
        delete result.messageKey
        return response.status(result.code).send(result)
      } else {
        result.success = false
        result.code = 422
        result.message = "Forgot password fail"
        result.messageKey = "FORGOT_PASSWORD_FAIL"
        return response.status(result.code).send(result)
      }
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Forgot password failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }
}
