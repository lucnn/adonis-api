import zaloConfig from 'Config/zalo';
import {HttpContextContract} from "@ioc:Adonis/Core/HttpContext";
import { URLSearchParams } from "url"
import User from "App/Models/User";
import VerifyCode from "App/Models/VerifyCode";
import CUtilsConfig from "Config/CUtilsConfig";
const isEmpty = require('lodash.isempty');
import Logger from "@ioc:Adonis/Core/Logger";
import SocialRegisterValidator from 'App/Validators/SocialRegisterValidator';
import { string } from '@ioc:Adonis/Core/Helpers'
var http = require('request');
export default class LoginSocialController {
    /**
    * @swagger
    * /facebook/redirect:
    *   get:
    *     tags:
    *       - Auth
    *     summary: Facebook redirect
    */
    public async redirectFacebook({ ally })
    {
        return ally.use('facebook').redirect((request) => {
            request.scopes(['email', 'public_profile', 'user_birthday'])
        })
    }
    /**
    * @swagger
    * /facebook/callback:
    *   get:
    *     tags:
    *       - Auth
    *     summary: Facebook callback
    */
    public async callbackFacebook({ ally, auth, response })
    {
        var dataUri = {
            status: CUtilsConfig.status.cancel.toString(),
            statusLoginSocial: '',
            email: '',
            name: '',
            token: ''
        }
        try {
            const facebook = ally.use('facebook')
            
            if (facebook.accessDenied()) {
                response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
            }

            if (facebook.stateMisMatch()) {
                response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
            }

            if (facebook.hasError()) {
                response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
            }

            const facebookUser = await facebook.user()
            var password = Math.random().toString(36).substr(2, 100000000);
            var userData = await User.query().where({'username': facebookUser.id}).first();
            var statusLoginSocial = CUtilsConfig.statusLoginSocial.isLogin
            if(!isEmpty(userData)){
                userData!.password = password
                await userData?.save()
            } else {
                var params = {
                    username: facebookUser.id,
                    email: facebookUser.email,
                    name: facebookUser.name,
                    password: password,
                    type: CUtilsConfig.userType.facebook
                }
                userData = await User.create(params)
                statusLoginSocial = CUtilsConfig.statusLoginSocial.firstLogin
            }

            const tokenInfo = await auth.use("api").attempt(facebookUser.id, password, {
                expiresIn: CUtilsConfig.token.expiresIn,
            });
            dataUri.status = CUtilsConfig.status.active.toString()
            dataUri.statusLoginSocial = statusLoginSocial.toString()
            dataUri.email = facebookUser.email
            dataUri.name = facebookUser.name 
            dataUri.token = tokenInfo.token
            response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
        } catch (error) {
            response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
        }
    }
    /**
    * @swagger
    * /google/redirect:
    *   get:
    *     tags:
    *       - Auth
    *     summary: Gooogle redirect
    */
    public async redirectGoogle({ ally })
    {
        return ally.use('google').redirect()
    }
    /**
    * @swagger
    * /google/callback:
    *   get:
    *     tags:
    *       - Auth
    *     summary: Google callback
    */
    public async callbackGoogle({ ally, response, auth })
    {
        var dataUri = {
            status: CUtilsConfig.status.cancel.toString(),
            statusLoginSocial: '',
            email: '',
            name: '',
            token: ''
        }
        try {
            const google = ally.use('google')
            if (google.accessDenied()) {
                response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
            }
        
            if (google.stateMisMatch()) {
                response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
            }
        
            if (google.hasError()) {
                response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
            }
        
            const googleUser = await google.user()
            var password = Math.random().toString(36).substr(2, 100000000);
            var userData = await User.query().where({'username': googleUser.id}).first();
            var statusLoginSocial = CUtilsConfig.statusLoginSocial.isLogin
            if(!isEmpty(userData)){
                userData!.password = password
                await userData?.save()
            } else {
                var params = {
                    username: googleUser.id,
                    email: googleUser.email,
                    name: googleUser.name,
                    password: password,
                    type: CUtilsConfig.userType.google
                }
                userData = await User.create(params)
                statusLoginSocial = CUtilsConfig.statusLoginSocial.firstLogin
            }
            const tokenInfo = await auth.use("api").attempt(googleUser.id, password, {
                expiresIn: CUtilsConfig.token.expiresIn,
            });

            dataUri.status = CUtilsConfig.status.active.toString()
            dataUri.statusLoginSocial = statusLoginSocial.toString()
            dataUri.email = googleUser.email
            dataUri.name = googleUser.name 
            dataUri.token = tokenInfo.token
            response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
        } catch (error) {
            response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
        }
    }

    /**
    * @swagger
    * /api/social/register:
    *   post:
    *     tags:
    *       - Auth
    *     summary: Login social
    *     security:
    *       - bearerAuth: []
    *     requestBody:
    *      required: true
    *      content:
    *        application/json:
    *          schema:
    *            $ref: '#/definitions/LoginSocial'
    *     responses:
    *       200:
    *         description: Kết quả khi thành công
    *         content:
    *          application/json:
    *            schema:
    *              $ref: '#/definitions/SuccessResponse'
    *              properties:
    *                data:
    *                  schema:
    *                  $ref: '#/definitions/UserInfo'
    *       400:
    *         description: Kết quả khi lỗi
    *         content:
    *          application/json:
    *            schema:
    *              $ref: '#/definitions/ErrorResponse'
    */

    public async register({ request, response, auth }: HttpContextContract) {
        try {
            await request.validate(SocialRegisterValidator)
            const params = request.body();
            let checkCode = await VerifyCode.query().where({
                'payload': params.phone, 
                'type': CUtilsConfig.verifyCodeType.registerSocial, 
                'status': CUtilsConfig.status.active, 
                'code': params.code})
            .first()
            if(isEmpty(checkCode)){
                return response.status(400).send({
                    success: false,
                    message: 'Authentication code is expired',
                    statusCode: 400
                })
            }

            const user = await auth.authenticate();

            user.name = params.name
            user.phone = params.phone
            user.birthday = params.birthday
            user.very_phone = CUtilsConfig.status.active
            user.save()
            let responseData = {
                success: true,
                message: 'success',
                data: {...user?.toJSON()}
              }
            return response.json(responseData)
        } catch (error) {
            Logger.error({ err: new Error(error) }, "Register failed");
            return response.status(400).send({
                success: false,
                statusCode: 400,
                message: error.message,
                errors: error
            })
        }
    }

    public async redirectZalo({response})
    {
        
        var codeVerifier = string.generateRandom(43)
        var dataUri = {
            app_id: zaloConfig.clientId,
            redirect_uri: zaloConfig.callbackUrl,
            state: codeVerifier,
        }
        var url = zaloConfig.zaloUrl + "?" + new URLSearchParams(dataUri).toString()
        response.redirect().toPath( url )
    }

    public async callbackZalo({ request, response, auth }: HttpContextContract )
    {
        var dataUri = {
            status: CUtilsConfig.status.cancel.toString(),
            statusLoginSocial: '',
            email: '',
            name: '',
            token: ''
        }
        try {
            let code = request.input("code");
            let option = {
                url: zaloConfig.zaloOauth,
                method: 'POST',
                headers: {
                    'secret_key': zaloConfig.clientSecret
                },
                form: {
                    code: code,
                    app_id: zaloConfig.clientId,
                    grant_type: "authorization_code"
                }
            }
            let isError = false;
            let accessToken;
            let res = await this.doRequest(option)
    
            if (res.error) {
                isError = true
            } else {
                accessToken = res.access_token
            }
            if (isError) {
                response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
            }
            //Get user infomation
            let userInfo = await this.doRequest({url: zaloConfig.zaloApiGraphMe + "?access_token=" + accessToken, method: "GET"})
            // console.log(userInfo)
            if (userInfo.error) {
                response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
            }
    
            var password = Math.random().toString(36).substr(2, 100000000);
            var userData = await User.query().where({'username': userInfo.id}).first();
            var statusLoginSocial = CUtilsConfig.statusLoginSocial.isLogin
            if(!isEmpty(userData)){
                userData!.password = password
                await userData?.save()
            } else {
                var params = {
                    username: userInfo.id,
                    email: userInfo.email,
                    name: userInfo.name,
                    password: password,
                    type: CUtilsConfig.userType.zalo
                }
                userData = await User.create(params)
                statusLoginSocial = CUtilsConfig.statusLoginSocial.firstLogin
            }
            const tokenInfo = await auth.use("api").attempt(userInfo.id, password, {
                expiresIn: CUtilsConfig.token.expiresIn,
            });
    
            dataUri.status = CUtilsConfig.status.active.toString()
            dataUri.statusLoginSocial = statusLoginSocial.toString()
            dataUri.name = userInfo.name 
            dataUri.token = tokenInfo.token
            response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
        } catch (error) {
            response.redirect().toPath( CUtilsConfig.redirectURI + "?" + new URLSearchParams(dataUri).toString())
        }
    }

    public doRequest(option)
    {
        return new Promise(function (resolve, reject) {
            http(option, function (error, res, body) {
                if (!error && res.statusCode == 200) {
                    resolve(JSON.parse(body));
                } else {
                    reject(error);
                }
            });
        });
    }
}