import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Banner from "App/Models/Banner";
import Logger from "@ioc:Adonis/Core/Logger";
const Help = require("App/Common/Help");
let helper = new Help()

export default class BannersController {
  /**
  * @swagger
  * /api/banners:
  *   get:
  *     tags:
  *       - Banners
  *     summary: List Banner
  *     security:
  *       - bearerAuth: []
  *     parameters:
  *       - name: order
  *         description: order sort
  *         in: query
  *         required: false
  *       - name: type
  *         description: type banner
  *         in: query
  *         required: false
  *       - name: position
  *         description: position banner
  *         in: query
  *         required: false
  *       - name: page
  *         description: page banner
  *         in: query
  *         required: false
  *       - name: limit
  *         description: limit banner
  *         in: query
  *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Banner'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async index({ request, response }: HttpContextContract) {
    try {
      let order = request.input("order")
      let type = request.input('type')
      let position = request.input('position')
      let page = request.input('page', 1)
      let limit = request.input('limit', 20)
      let status = 1 // Chỉ lấy bản ghi active
      let params = {}
      if (order) params["order"] = order
      if (type) params["type"] = type
      if (position) params["position"] = position
      if (status) params["status"] = status
      const banners = await Banner.query().preload("event").where(params).orderBy('created_at', 'desc').paginate(page, limit);
      let bannersData = banners.toJSON().data
      let bannersMeta = banners.toJSON().meta
      bannersMeta = helper.formatMeta(bannersMeta)
      const bannerJson = bannersData.map((bannersData) => bannersData.serialize())
      Logger.info({ banners: bannersData.length }, "Get list banners successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: bannerJson,
        meta: bannersMeta
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list banners failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api/banners/{id}:
   *   get:
   *     tags:
   *       - Banners
   *     summary: Detail Banner
   *     security:
   *       - bearerAuth: []
  *     parameters:
  *       - name: id
  *         description: id Banner
  *         in: path
  *         required: true
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Banner'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async show({ request, response }: HttpContextContract) {
    try {
      let id = request.param("id")
      const banner = await Banner.find(id);
      if (banner) {
        await banner.preload("event")
      }
      let responseData = {
        success: true,
        message: 'success',
        data: banner ? banner?.toJSON() : null
      }
      Logger.info({ banner: 1 }, "Get banner successfully");
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get banner failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }
}
