import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Order from "App/Models/Order";
import Campaign from "App/Models/Campaign";
import Logger from "@ioc:Adonis/Core/Logger";
import CUtilsConfig from "Config/CUtilsConfig";
import OrderValidator from "App/Validators/OrderValidator";
import Deal from "App/Models/Deal";
import { DateTime } from "luxon";
import moment from "moment";
import pick from "lodash/pick";
import PointHistory from "App/Models/PointHistory";
import User from "App/Models/User";
import omit from "lodash/omit";
const Help = require("App/Common/Help");
let helper = new Help();
const isEmpty = require("lodash.isempty");
export default class OrderController {
  /**
   * @swagger
   * /api/orders:
   *   get:
   *     tags:
   *       - Orders
   *     summary: List order
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: status
   *         description: status order
   *         in: query
   *         required: false
   *       - name: page
   *         description: page order
   *         in: query
   *         required: false
   *       - name: limit
   *         description: limit order
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Order'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async index({ request, response, auth }: HttpContextContract) {
    try {
      const user = await auth.authenticate();
      let page = request.input("page", 1);
      let limit = request.input("limit", 20);
      let params = { user_id: user.id };
      const orders = await Order.query()
        .preload("campaign")
        .preload("deal")
        .whereNull("deleted_at")
        .where("status", ">", CUtilsConfig.orderStatus.initiated)
        .where(params)
        .orderBy("created_at", "desc")
        .paginate(page, limit);
      let ordersData = orders.toJSON().data;
      let ordersMeta = orders.toJSON().meta;
      ordersMeta = helper.formatMeta(ordersMeta);
      const ordersJson = ordersData.map((ordersData) => {
        const data = ordersData.serialize();
        if (
          ordersData.status === CUtilsConfig.orderStatus.pending &&
          ordersData.deal
        ) {
          data.status =
            ordersData.deal.status === CUtilsConfig.dealStatus.matched
              ? CUtilsConfig.orderStatus.matched
              : CUtilsConfig.orderStatus.unmatched;
        }
        return {
          ...omit(data, ["deal"]),
          campaign: pick(data.campaign, ["name", "thumbnail"]),
        };
      });
      Logger.info({ order: ordersData.length }, "Get list order successfully");
      let responseData = {
        success: true,
        message: "success",
        data: ordersJson,
        meta: ordersMeta,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list order failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api/orders/{id}:
   *   get:
   *     tags:
   *       - Orders
   *     summary: Detail Order
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: id
   *         description: id category
   *         in: path
   *         required: true
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Order'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async show({ request, response, auth }: HttpContextContract) {
    try {
      const user = await auth.authenticate();
      let id = request.param("id");
      const order = await Order.query()
        .where({ id: id, user_id: user.id })
        .first();
      if (!order) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Order not found",
          messageKey: "ORDER_NOT_FOUND",
          data: null,
        });
      }
      await order.preload("campaign");
      await order.preload("userAddress");
      await order.preload("deal");
      const data = order.toJSON();
      if (data.status === CUtilsConfig.orderStatus.pending && data.deal) {
        data.status =
          data.deal.status === CUtilsConfig.dealStatus.matched
            ? CUtilsConfig.orderStatus.matched
            : CUtilsConfig.orderStatus.unmatched;
      }
      data.deal = pick(data.deal, ["id", "is_open"])
      Logger.info({ order: 1 }, "Get order successfully");
      let responseData = {
        success: true,
        message: "success",
        data,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get order failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api/orders:
   *   post:
   *     tags:
   *       - Orders
   *     summary: Create order
   *     security:
   *       - bearerAuth: []
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/definitions/OrderForm'
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Order'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async store({ request, response, auth }: HttpContextContract) {
    try {
      await request.validate(OrderValidator);
      const user = (await auth.authenticate()) as User;
      const order = new Order();
      var totalPrice = 0;
      const options = request.input("options");
      const pointDiscount = request.input("pointDiscount");
      order.campaignId = request.input("campaignId");
      const campaign = await Campaign.find(order.campaignId);
      if (!campaign) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "campaignId invalid",
        });
      }
      order.pointDiscount = 0;
      if (pointDiscount) {
        if (pointDiscount > user.point) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            message: "pointDiscount invalid",
          });
        }
        order.pointDiscount = pointDiscount;
      }
      order.dealId = request.input("dealId");
      if (order.dealId) {
        const deal = await Deal.find(order.dealId);
        const now = DateTime.local();
        if (!deal || deal.status === CUtilsConfig.status.cancel) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            message: "Deal not found or full",
          });
        }
        const orderPending = await Order.query()
          .where({
            deal_id: order.dealId,
            status: CUtilsConfig.status.active,
          })
          .where("expire_at", ">", now.toISO())
          .first();
        if (orderPending) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            message: "There is another transaction in progress",
          });
        }
        if (
          deal.status === CUtilsConfig.status.warning &&
          deal.makerId !== user.id
        ) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            message: "User invalid warning",
          });
        }
        if (
          deal.status === CUtilsConfig.status.active &&
          deal.makerId === user.id
        ) {
          return response.status(400).send({
            success: false,
            statusCode: 400,
            message: "User invalid active",
          });
        }
      }
      // TODO: Need to check inventory
      if (!isEmpty(options)) {
        const campaignPrice = order.dealId
          ? campaign.dealPrice
          : campaign.originalPrice;
        const campaignOption = campaign.options
          ? JSON.parse(campaign.options)
          : [];
        order.quantity = 0;
        for (const element of options) {
          const dataItem =
            campaignOption.data.find((x: any) => x.id === element.id) ||
            campaignOption.data[0];
          totalPrice += campaignPrice * element.quantity;
          order.quantity += element.quantity;
          if (dataItem.price) {
            totalPrice += dataItem.price * element.quantity;
          }
        }
      }
      order.userAddressId = request.input("userAddressId");
      order.options = JSON.stringify(request.input("options"));
      order.status = CUtilsConfig.orderStatus.initiated;
      order.totalPrice = totalPrice;
      order.shippingFee = request.input("shippingFee");
      order.grandTotal = totalPrice + order.shippingFee - order.pointDiscount;
      order.paymentMethod = request.input("paymentMethod");
      order.userId = user.id;
      order.expireAt = DateTime.fromJSDate(moment().add(5, "m").toDate());
      if (order.pointDiscount) {
        const pointHistory = new PointHistory();
        pointHistory.userId = user.id;
        pointHistory.value = Math.round(order.pointDiscount);
        pointHistory.type = CUtilsConfig.pointHistoryType.usageForDiscount;
        user.point -= pointHistory.value;
        await user.save();
        await pointHistory.save();
      }
      await order.save();
      const orderJson = order.serialize();
      Logger.info({ order: order.id }, "Create order successfully");
      let responseData = {
        success: true,
        message: "success",
        data: orderJson,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Create order failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  public async cancel({ request, response, auth }: HttpContextContract) {
    try {
      const user = (await auth.authenticate()) as User;
      const id = request.param("id");
      const order = await Order.query()
        .where({ id: id, user_id: user.id })
        .first();
      if (!order) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Order not found",
          messageKey: "ORDER_NOT_FOUND",
          data: null,
        });
      }
      if (
        ![
          CUtilsConfig.orderStatus.pending,
          CUtilsConfig.orderStatus.matched,
        ].includes(order.status)
      ) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Cannot cancel this order",
          data: null,
        });
      }

      if(order.pointDiscount > 0) {
        const pointHistory = new PointHistory();
        pointHistory.value = order.pointDiscount;
        pointHistory.type = CUtilsConfig.pointHistoryType.returnForCancellation;
        pointHistory.userId = user.id;
        await pointHistory.save();
      
        user.point += order.pointDiscount;
        await user.save();
      }

      order.status = CUtilsConfig.orderStatus.cancelled;
      await order.save();
      const responseData = {
        success: true,
        message: "success",
        data: order?.toJSON(),
      };
      return response.json(responseData);
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  public async complete({ request, response, auth }: HttpContextContract) {
    try {
      const user = (await auth.authenticate()) as User;
      const id = request.param("id");
      const order = await Order.query()
        .where({ id: id, user_id: user.id })
        .first();
      if (!order) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Order not found",
          messageKey: "ORDER_NOT_FOUND",
          data: null,
        });
      }
      if (![CUtilsConfig.orderStatus.onDelivery].includes(order.status)) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Cannot complete this order",
          data: null,
        });
      }
      order.status = CUtilsConfig.orderStatus.completed;
      const deal = await Deal.query()
        .where({
          id: order.dealId,
          maker_id: user.id,
          status: CUtilsConfig.dealStatus.matched,
        })
        .first();
      if (deal) {
        const pointHistory = new PointHistory();
        pointHistory.userId = user.id;
        pointHistory.value = Math.round(order.grandTotal * 0.01);
        pointHistory.type = CUtilsConfig.pointHistoryType.openDealIncentive;
        user.point += pointHistory.value;
        // TODO: Using transaction
        await user.save();
        await pointHistory.save();
      }
      await order.save();
      const responseData = {
        success: true,
        message: "success",
        data: order?.toJSON(),
      };
      return response.json(responseData);
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  public async refund({ request, response, auth }: HttpContextContract) {
    try {
      const user = (await auth.authenticate()) as User;
      const id = request.param("id");
      const order = await Order.query()
        .where({ id: id, user_id: user.id })
        .first();
      if (!order) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Order not found",
          messageKey: "ORDER_NOT_FOUND",
          data: null,
        });
      }
      if (![CUtilsConfig.orderStatus.completed].includes(order.status)) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Cannot complete this order",
          data: null,
        });
      }
      order.status = CUtilsConfig.orderStatus.refundProcess;
      const deal = await Deal.query()
        .where({
          id: order.dealId,
          maker_id: user.id,
          status: CUtilsConfig.dealStatus.matched,
        })
        .first();
      if (deal) {
        const pointHistory = new PointHistory();
        pointHistory.userId = user.id;
        pointHistory.value = -Math.round(order.grandTotal * 0.01);
        pointHistory.type = CUtilsConfig.pointHistoryType.refundProcess;
        user.point += pointHistory.value;
        // TODO: Using transaction
        await user.save();
        await pointHistory.save();
      }
      await order.save();
      const responseData = {
        success: true,
        message: "success",
        data: order?.toJSON(),
      };
      return response.json(responseData);
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }
}
