import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Campaign from "App/Models/Campaign";
import Order from "App/Models/Order";
import { createHmac } from "crypto";
import moment from "moment";
import { stringify } from "qs";
import Env from "@ioc:Adonis/Core/Env";
import Deal from "App/Models/Deal";
import CUtilsConfig from "Config/CUtilsConfig";
import { DateTime } from "luxon";

type CategoryOption = "separated" | "combination";
type OrderOption = {
  id: string;
  quantity: number;
};

type CampaignOption = {
  type: CategoryOption;
  count: number;
  value: any[];
  data: any[];
};

export default class PaymentsController {
  public async vnPayCreateUrl({
    request,
    response,
    auth,
  }: HttpContextContract) {
    const user = await auth.authenticate();
    const order = await Order.find(+request.param("orderId"));
    if (
      order === null ||
      order.userId !== user.id ||
      order.status !== CUtilsConfig.orderStatus.initiated
    ) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: "Invalid order id",
      });
    }
    let params = {
      vnp_Version: "2.1.0",
      vnp_Command: "pay",
      vnp_OrderType: "billpayment",
      vnp_Locale: "vn",
      vnp_CurrCode: "VND",
      vnp_TmnCode: Env.get("VNPAY_TMN_CODE"),
      vnp_ReturnUrl: `${Env.get("DOMAIN")}${Env.get("VNPAY_RETURN_URL")}`,
      vnp_IpAddr: request.ip(),
      vnp_CreateDate: moment().format("YYYYMMDDHHmmss"),
      vnp_Amount: order.grandTotal * 100,
      vnp_TxnRef: order.id,
      vnp_OrderInfo: `Order ${order.id}`,
    };
    params = this.sortObject(params);
    params["vnp_SecureHash"] = await this.getSignData(params);
    const vnpUrl = `${Env.get("VNPAY_PAYMENT_URL")}?${stringify(params, {
      encode: false,
    })}`;
    response.json({ vnpUrl });
  }

  async vnPayIPN({ request, response }: HttpContextContract) {
    let params = request.qs();
    const secureHash = params["vnp_SecureHash"];
    delete params["vnp_SecureHash"];
    delete params["vnp_SecureHashType"];
    params = this.sortObject(params);
    const signed = await this.getSignData(params);
    if (secureHash !== signed)
      return { Message: "Invalid Checksum", RspCode: "97" };
    const rspCode = params["vnp_ResponseCode"];
    const orderId = +params["vnp_TxnRef"];
    const order = await Order.find(orderId);
    if (rspCode !== "00") {
      if (order) {
        order.status = CUtilsConfig.orderStatus.paymentFailed;
        await order.save();
      }
      return { RspCode: "00", Message: "Confirm Success" };
    }
    if (!order) return { Message: "Order Not Found", RspCode: "01" };
    const transactionAmount = +params["vnp_Amount"] / 100;
    if (transactionAmount !== order.grandTotal)
      return { Message: "Invalid amount", RspCode: "04" };
    if (order.status !== CUtilsConfig.orderStatus.initiated)
      return { Message: "Order already confirmed", RspCode: "02" };
    try {
      order.status = CUtilsConfig.orderStatus.pending;
      const campaign = await Campaign.find(order.campaignId);
      if (!campaign) throw new Error();
      await order.save();
      const orderOptions: OrderOption[] = JSON.parse(order.options);
      const campaignOption: CampaignOption = JSON.parse(campaign.options);
      campaignOption.data.forEach((x) => {
        const item = orderOptions.find((y) => y.id === x.id);
        if (item) x.amount -= item.quantity;
      });
      campaign.options = JSON.stringify(campaignOption);
      campaign.amountOfSales += order.quantity;
      await campaign.save();
      if (order.dealId) {
        const deal = await Deal.find(order.dealId);
        if (!deal) throw new Error();
        if (deal.status === CUtilsConfig.dealStatus.pending) {
          deal.status = CUtilsConfig.dealStatus.active;
          deal.expireAt = DateTime.fromJSDate(moment().add(5, "m").toDate());
        } else {
          deal.status = CUtilsConfig.dealStatus.matched;
        }
        await deal.save();
      }
      return response.json({ RspCode: "00", Message: "Confirm Success" });
    } catch (error) {
      return response.json({ RspCode: "99", Message: "Unknown error" });
    }
  }

  async vnPayReturn({ request, response }: HttpContextContract) {
    let params = request.qs();
    const secureHash = params["vnp_SecureHash"];
    delete params["vnp_SecureHash"];
    delete params["vnp_SecureHashType"];
    params = this.sortObject(params);
    const signed = await this.getSignData(params);
    if (secureHash !== signed)
      return response.redirect(
        this.getRedirectUrl({
          statusCode: 97,
        })
      );
    const orderId = +params["vnp_TxnRef"];
    // const order = await Order.find(orderId);
    response.redirect(
      this.getRedirectUrl({
        statusCode: params["vnp_ResponseCode"],
        orderId,
      })
    );
  }

  private getRedirectUrl(params: any) {
    return `${Env.get("VNPAY_CALLBACK_URL")}?${stringify(params, {
      encode: true,
    })}`;
  }

  private async getSignData(params: any) {
    const signData = stringify(params, { encode: false });
    const hmac = createHmac("sha512", Env.get("VNPAY_SECRET"));
    return hmac.update(Buffer.from(signData, "utf-8")).digest("hex");
  }

  private sortObject(obj: any): any {
    const sorted = {};
    const keys = Object.keys(obj).sort();
    keys.forEach((key) => {
      sorted[key] = encodeURIComponent(obj[key]).replace(/%20/g, "+");
    });
    return sorted;
  }
}
