import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import VerifyCode from "App/Models/VerifyCode";
import User from "App/Models/User";
import Logger from "@ioc:Adonis/Core/Logger";
import VerifyCodeValidator from 'App/Validators/VerifyCodeValidator';
import PhoneNumberValidator from 'App/Validators/PhoneNumberValidator';
const isEmpty = require('lodash.isempty')
import CUtilsConfig from "Config/CUtilsConfig";

const listType = Object.values(CUtilsConfig.verifyCodeType);

export default class VeryCodeController {
    /**
     * @swagger
     * /api/verify:
     *   post:
     *     tags:
     *       - Verify
     *     summary: Verify API
     *     requestBody:
     *      required: true
     *      content:
     *        application/json:
     *          schema:
     *            properties:
     *              phone:
     *                description: Phone number to verify
     *                type: string
     *                required: true
     *              code:
     *                description: code to verify
     *                type: string
     *                required: true
     *              type:
     *                description: type to verify
     *                type: number
     *                required: false
     *     responses:
     *       200:
     *         description: Kết quả khi thành công
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/SuccessResponse'
     *       400:
     *         description: Kết quả khi lỗi
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/ErrorResponse'
     */
    public async verify({ request, response }: HttpContextContract) {
        try {
            let responseData = {
                success: false,
                statusCode: 400,
                message: 'Authentication code is wrong',
                messageKey: "CODE_IS_WRONG",
                data: null
            }
            await request.validate(VerifyCodeValidator)

            const phone = request.input("phone");
            const code = request.input("code");
            const type = request.input("type", 0);

            if (!listType.includes(type)) {
                responseData.message = "Type not found."
                responseData.messageKey = "TYPE_NOT_FOUND"
                return response.status(400).send(responseData)
            }
        /**
         * Create a new user
         */
            const verifyData = await VerifyCode.query().where({
                'payload': phone,
                'status': 0,
                type: type
            }).first()
            if (!isEmpty(verifyData)) {
                let expiredAt = verifyData!.expiredAt
                let timeNow = Math.floor(Date.now() / 1000)
                let lockCount = verifyData!.lockCount ?? 0
                let codeVerifyData = verifyData!.code
                let lockTime = verifyData!.lockTime
                if (lockTime > Math.floor(Date.now() / 1000)) {
                    responseData.message = "Authentication code is expired. Please wait for 10 minutes then try again."
                    responseData.messageKey = "LOCK_10_MINUTES"
                    return response.status(400).send(responseData)
                }
                if (codeVerifyData != code) {
                    let params = {
                        'payload': phone,
                        'status': 0
                    }
                    if (lockCount >= 4) {
                        let lockTime = 600
                        responseData.success = false
                        responseData.messageKey = "LOCK_10_MINUTES"
                        responseData.message = "Authentication code is expired. Please wait for 10 minutes then try again."

                        let dataUpdate = {
                            'lock_time': Math.floor(Date.now() / 1000) + lockTime,
                            'lock_count': 0
                        }
                        await this.updateVerifyCode(params, dataUpdate)
                        return response.status(400).send(responseData)
                    }
                    let dataUpdate = {
                        'lock_count': lockCount + 1
                    }
                    await this.updateVerifyCode(params, dataUpdate)
                    return response.status(400).send(responseData)
                } else if (timeNow > expiredAt) {
                    responseData.message = "Authentication code is expired."
                    responseData.messageKey = "CODE_IS_EXPIRED"
                    return response.status(400).send(responseData)
                } else {
                    let userData = await User.query().where({
                        'phone': phone,
                        'very_phone': 1
                    }).first();
                    if (!isEmpty(userData) && type != 1) {
                        responseData.message = "Phone number is verified"
                        responseData.messageKey = "PHONE_IS_VERIFIED"
                        return response.status(400).send(responseData)
                    } else {
                        let u = Date.now().toString(16)+Math.random().toString(16)+'0'.repeat(16);
                        let token_very = [u.substr(0,8), u.substr(8,4), '4000-8' + u.substr(13,3), u.substr(16,12)].join('-');
                        // let expiredAt = 120
                        let expiredAt = 500
                        let params = {
                            'payload': phone,
                            'status': 0,
                            'code': code,
                        }
                        let dataUpdate = {
                            'status': 1,
                            token_very: token_very,
                            expired_at: Math.floor(Date.now() / 1000) + expiredAt
                        }
                        responseData.success = true
                        responseData.statusCode = 200
                        responseData.message = "Success"
                        delete responseData.messageKey
                        responseData.data = {
                            token_very: token_very,
                            expired_at: expiredAt
                        }
                        await this.updateVerifyCode(params, dataUpdate)
                        return response.status(200).send(responseData)
                    }
                }
            } else {
                return response.status(400).send(responseData)
            }
        } catch (error) {
            Logger.error({ err: new Error(error) }, "Verify failed");
            return response.status(400).send({
                success: false,
                statusCode: 400,
                message: error.message,
                errors: error
            })
        }
    }

    /**
     * @swagger
     * /api/request-code:
     *   post:
     *     tags:
     *       - Verify
     *     summary: Request Code API
     *     requestBody:
     *      required: true
     *      content:
     *        application/json:
     *          schema:
     *            properties:
     *              phone:
     *                description: Phone number to verify
     *                type: string
     *                required: true
     *              type:
     *                description: Type to verify
     *                type: number
     *                required: false
     *     responses:
     *       200:
     *         description: Kết quả khi thành công
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/SuccessResponse'
     *              properties:
     *                data:
     *                  schema:
     *                  $ref: '#/definitions/ResCode'
     *       400:
     *         description: Kết quả khi lỗi
     *         content:
     *          application/json:
     *            schema:
     *              $ref: '#/definitions/ErrorResponse'
     */
     public async sendOtpToPhone({ request, response }: HttpContextContract) {
        try {
            await request.validate(PhoneNumberValidator)
            const phone = request.input("phone");
            const type = request.input("type", 0);
            let result = {
                code: 400,
                message: 'Type not found.',
                messageKey: "TYPE_NOT_FOUND",
                data: null
            }
            if (!listType.includes(type)) {
                return response.status(result.code).send(result)
            }

            if (type == 1) {
                const user = await User.query().where({
                    'phone': phone
                }).first()

                if (isEmpty(user)) {
                    result.message = "The account does not exist. Please enter a valid user information",
                    result.messageKey = "USER_NOT_EXIST"
                    return response.status(result.code).send(result)
                }

            }


            /**
             * Create a new code
             */
            await VerifyCode.query().where({
                'payload': phone,
                'status': 0
            }).update({
                'status': 2 //hủy mã trước đó
            })
            let expiredAt = 120
            // let expiredAt = 500
            const verifyCode = new VerifyCode();
            verifyCode.payload = phone;
            verifyCode.code = '123456';
            verifyCode.type = type
            // verifyCode.code = Math.floor(Math.random() * 1000000).toString()
            verifyCode.expiredAt = Math.floor(Date.now() / 1000) + expiredAt

            await verifyCode.save();
            Logger.info({ user: verifyCode.payload }, "Send code successfully");
            let responseData = {
                success: true,
                message: 'success',
                data: {
                    expiredAt: expiredAt,

                    //Trả thêm để debug
                    code: verifyCode.code
                    //Trả thêm để debug
                }
            }
            return response.json(responseData)
        } catch (error) {
            Logger.error({ err: new Error(error) }, "Send code failed");
            return response.status(400).send({
                success: false,
                statusCode: 400,
                message: error.message,
                errors: error
            })
        }
    }

    public async updateVerifyCode(params, dataUpdate)
    {
        return await VerifyCode.query().where(params).update(dataUpdate)
    }
}
