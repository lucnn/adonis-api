import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Logger from "@ioc:Adonis/Core/Logger";
import Campaign from "App/Models/Campaign";
import Category from "App/Models/Category";
import Deal from "App/Models/Deal";
import Favorite from "App/Models/Favorite";
import RecentView from "App/Models/RecentView";
import CUtilsConfig from "Config/CUtilsConfig";
import { DateTime } from "luxon";
const isEmpty = require("lodash.isempty");
const Help = require("App/Common/Help");
let helper = new Help();

export default class CampaignsController {
  /**
   * @swagger
   * /api/campaigns:
   *   get:
   *     tags:
   *       - Campaigns
   *     summary: List Campaign
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: keyword
   *         description: từ khoá tìm kiếm
   *         in: query
   *         required: false
   *       - name: megaDeal
   *         description: mega deal
   *         in: query
   *         required: false
   *       - name: categoryId
   *         description: category id campaign
   *         in: query
   *         required: false
   *       - name: limit
   *         description: limit campaign
   *         in: query
   *         required: false
   *       - name: page
   *         description: page campaign
   *         in: query
   *         required: false
   *       - name: isHottestDeals
   *         description: isHottestDeals = 1 or 0
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Campaign'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async index({ auth, request, response }: HttpContextContract) {
    try {
      const user = await auth.authenticate();
      let keyword = request.input("keyword");
      let categoryId = request.input("categoryId");
      let megaDeal = request.input("megaDeal");
      let status = CUtilsConfig.status.active; // Chỉ lấy bản ghi active
      let isHottestDeals = request.input("isHottestDeals");
      let page = request.input("page", 1);
      let limit = request.input("limit", 20);
      let params = {};
      if (categoryId) params["category_id"] = categoryId;
      if (megaDeal) params["mega_deal"] = megaDeal;
      if (status) params["status"] = status;
      const campaignQuery = Campaign.query().where(params);
      if (keyword) {
        keyword = helper.replaceAll(keyword, "_", "\\_");
        keyword = helper.replaceAll(keyword, "%", "\\%");
        campaignQuery.andWhere("name", "like", "%" + keyword + "%");
        // campaignQuery.andWhere((query) => {
        //   query
        //     .where('name', 'like', '%'+keyword+'%')
        //     .orWhere('content', 'like', '%'+keyword+'%')
        // })
      }
      if (megaDeal == CUtilsConfig.megaDeal.active) {
        const timeCurrent = Math.floor(Date.now() / 1000);
        campaignQuery.andWhere("mega_deal_duration_start", "<=", timeCurrent);
        campaignQuery.andWhere("mega_deal_duration_end", ">=", timeCurrent);
      }
      if (isHottestDeals) {
        campaignQuery.orderBy("view_count", "desc");
      } else {
        campaignQuery.orderBy("createdAt", "desc");
      }
      const campaigns = await campaignQuery
        .preload("category")
        .paginate(page, limit);
      let campaignsData = campaigns.toJSON().data;
      let campaignsMeta = campaigns.toJSON().meta;
      campaignsMeta = helper.formatMeta(campaignsMeta);
      const campaignJson = campaignsData.map(async (campaignsData) => {
        const campaignsDataNew = campaignsData.serialize();
        var favorite = 0;
        let checkCode = await Favorite.query()
          .where({ user_id: user.id, campaign_id: campaignsDataNew.id })
          .first();
        if (!isEmpty(checkCode)) {
          favorite = 1;
        }
        campaignsDataNew.favorite = favorite;
        if (campaignsDataNew.mega_deal) {
          const count = await Deal.query()
            .where("campaign_id", campaignsData.id)
            .where(
              "created_at",
              ">",
              DateTime.fromSeconds(campaignsData.megaDealDurationStart).toISO()
            )
            .where(
              "created_at",
              "<",
              DateTime.fromSeconds(campaignsData.megaDealDurationEnd).toISO()
            )
            .where("status", CUtilsConfig.status.cancel)
            .count("id");
          campaignsDataNew.participation_rate = count[0]["count(`id`)"] * 2;
          campaignsDataNew.accomplishment_rate =
            campaignsDataNew.target_number_people;
        }
        if (campaignsDataNew.images) {
          campaignsDataNew.images = JSON.parse(campaignsDataNew.images);
        }
        if (campaignsDataNew.options) {
          campaignsDataNew.options = JSON.parse(campaignsDataNew.options);
        }
        if (campaignsDataNew.benefits_tag) {
          campaignsDataNew.benefits_tag = JSON.parse(
            campaignsDataNew.benefits_tag
          );
        }
        return campaignsDataNew;
      });
      const campaignJsonNew = await Promise.all(campaignJson);
      Logger.info(
        { campaigns: campaigns.length },
        "Get list campaigns successfully"
      );
      let responseData = {
        success: true,
        message: "success",
        data: campaignJsonNew,
        meta: campaignsMeta,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list campaigns failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api/campaigns/{id}:
   *   get:
   *     tags:
   *       - Campaigns
   *     summary: Detail Campaign
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: id
   *         description: id Campaign
   *         in: path
   *         required: true
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Campaign'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async show({ auth, request, response }: HttpContextContract) {
    try {
      const user = await auth.authenticate();
      let id = request.param("id");
      const campaign = await Campaign.find(id);
      if (campaign) {
        await campaign.preload("category");
      } else {
        return response.status(400).json({
          statusCode: 400,
          message: "Campaign not found.",
          data: null,
        });
      }
      campaign.viewCount = (campaign.viewCount ?? 0) + 1;
      campaign.save();
      let recentView = await RecentView.query()
        .where({ user_id: user.id, campaign_id: campaign.id })
        .first();
      if (isEmpty(recentView)) {
        const recentView = new RecentView();
        recentView.userId = user.id;
        recentView.campaignId = campaign.id;
        recentView.viewCount = 1;
        recentView.save();
      } else if (recentView) {
        recentView.viewCount++;
        recentView.save();
      }

      const campaignsData = campaign.serialize();
      var favorite = 0;
      let checkCode = await Favorite.query()
        .where({ user_id: user.id, campaign_id: campaign.id })
        .first();
      if (!isEmpty(checkCode)) {
        favorite = 1;
      }
      campaignsData.favorite = favorite;
      if (campaignsData.mega_deal) {
        const count = await Deal.query()
          .where("campaign_id", campaign.id)
          .where(
            "created_at",
            ">",
            DateTime.fromSeconds(campaign.megaDealDurationStart).toISO()
          )
          .where(
            "created_at",
            "<",
            DateTime.fromSeconds(campaign.megaDealDurationEnd).toISO()
          )
          .where("status", CUtilsConfig.status.cancel)
          .count("id");
        campaignsData.participation_rate = count[0]["count(`id`)"] * 2;
        campaignsData.accomplishment_rate = campaignsData.target_number_people;
      }
      const count = await Deal.query()
        .where("campaign_id", campaign.id)
        .where("status", CUtilsConfig.status.cancel)
        .count("id");
      campaignsData.success_people = count[0]["count(`id`)"] * 2;
      if (campaignsData.images) {
        campaignsData.images = JSON.parse(campaignsData.images);
      }
      if (campaignsData.options) {
        campaignsData.options = JSON.parse(campaignsData.options);
      }
      if (campaignsData.benefits_tag) {
        campaignsData.benefits_tag = JSON.parse(campaignsData.benefits_tag);
      }
      let responseData = {
        success: true,
        message: "success",
        data: campaignsData,
      };
      Logger.info({ campaign: 1 }, "Get campaign successfully");
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get campaign failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api/campaigns/favorite:
   *   post:
   *     tags:
   *       - Campaigns
   *     summary: favorite Campaign
   *     security:
   *       - bearerAuth: []
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/definitions/CampaignFavorite'
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async favorite({ auth, response, request }: HttpContextContract) {
    try {
      const user = await auth.authenticate();
      let campaign_id = request.input("campaignId");
      let status = request.input("status", 0);
      let favorite = await Favorite.query()
        .where({ user_id: user.id, campaign_id })
        .first();
      if (isEmpty(favorite)) {
        if (status == 1) {
          const favorite = new Favorite();
          favorite.userId = user.id;
          favorite.campaignId = campaign_id;
          favorite.save();
        }
      } else {
        if (status == 0) {
          await Favorite.query().where("id", favorite!.id).delete();
        }
      }
      Logger.info({ campaign: campaign_id }, "favorite successfully");
      let responseData = {
        success: true,
        message: "success",
        data: null,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "favorite failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api/campaigns/favorite-list:
   *   get:
   *     tags:
   *       - Campaigns
   *     summary: favorite List Campaign
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: keyword
   *         description: từ khoá tìm kiếm
   *         in: query
   *         required: false
   *       - name: megaDeal
   *         description: mega deal
   *         in: query
   *         required: false
   *       - name: categoryId
   *         description: category id campaign
   *         in: query
   *         required: false
   *       - name: limit
   *         description: limit campaign
   *         in: query
   *         required: false
   *       - name: page
   *         description: page campaign
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Campaign'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async favoriteList({ request, response, auth }: HttpContextContract) {
    try {
      const user = await auth.authenticate();
      let keyword = request.input("keyword");
      let categoryId = request.input("categoryId");
      let megaDeal = request.input("megaDeal");
      let status = CUtilsConfig.status.active; // Chỉ lấy bản ghi active
      let page = request.input("page", 1);
      let limit = request.input("limit", 20);
      let params = {};
      if (categoryId) params["category_id"] = categoryId;
      if (megaDeal) params["mega_deal"] = megaDeal;
      if (status) params["status"] = status;
      // const campaignQuery = Campaign.query().where(params).andWhereIn(
      //   'id',
      //   (query) => query.from('favorites').select('campaign_id').where('user_id', user.id)
      // )
      // if(keyword){
      //   keyword = helper.replaceAll(keyword, '_','\\_')
      //   keyword = helper.replaceAll(keyword, '%','\\%')
      //   campaignQuery.andWhere('name', 'like', '%'+keyword+'%')
      //   // campaignQuery.andWhere((query) => {
      //   //   query
      //   //     .where('name', 'like', '%'+keyword+'%')
      //   //     .orWhere('content', 'like', '%'+keyword+'%')
      //   // })
      // }
      // const campaigns = await campaignQuery.preload("category").orderBy('created_at', 'desc').paginate(page, limit);

      const campaignQuery = Campaign.query()
        .innerJoin("favorites", "campaigns.id", "=", "favorites.campaign_id")
        .select("campaigns.*")
        .where(params)
        .andWhere("favorites.user_id", user.id);
      if (keyword) {
        keyword = helper.replaceAll(keyword, "_", "\\_");
        keyword = helper.replaceAll(keyword, "%", "\\%");
        campaignQuery.andWhere("name", "like", "%" + keyword + "%");
      }
      const campaigns = await campaignQuery
        .preload("category")
        .orderBy("favorites.created_at", "desc")
        .paginate(page, limit);

      let campaignsData = campaigns.toJSON().data;
      let campaignsMeta = campaigns.toJSON().meta;
      campaignsMeta = helper.formatMeta(campaignsMeta);
      const campaignJson = campaignsData.map(async (campaignsData) => {
        const campaignsDataNew = campaignsData.serialize();
        var favorite = 0;
        let checkCode = await Favorite.query()
          .where({ user_id: user.id, campaign_id: campaignsDataNew.id })
          .first();
        if (!isEmpty(checkCode)) {
          favorite = 1;
        }
        campaignsDataNew.favorite = favorite;
        return campaignsDataNew;
      });
      const campaignJsonNew = await Promise.all(campaignJson);
      Logger.info(
        { campaigns: campaigns.length },
        "Get list campaigns successfully"
      );
      let responseData = {
        success: true,
        message: "success",
        data: campaignJsonNew,
        meta: campaignsMeta,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list campaigns failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api/campaigns/recent-views:
   *   get:
   *     tags:
   *       - Campaigns
   *     summary: recent Views Campaign
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: keyword
   *         description: từ khoá tìm kiếm
   *         in: query
   *         required: false
   *       - name: megaDeal
   *         description: mega deal
   *         in: query
   *         required: false
   *       - name: categoryId
   *         description: category id campaign
   *         in: query
   *         required: false
   *       - name: limit
   *         description: limit campaign
   *         in: query
   *         required: false
   *       - name: page
   *         description: page campaign
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Campaign'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async recentViews({ request, response, auth }: HttpContextContract) {
    try {
      const user = await auth.authenticate();
      let keyword = request.input("keyword");
      let categoryId = request.input("categoryId");
      let megaDeal = request.input("megaDeal");
      let status = CUtilsConfig.status.active; // Chỉ lấy bản ghi active
      let page = request.input("page", 1);
      let limit = request.input("limit", 20);
      let params = {};
      if (categoryId) params["category_id"] = categoryId;
      if (megaDeal) params["mega_deal"] = megaDeal;
      if (status) params["status"] = status;
      const campaignQuery = Campaign.query()
        .innerJoin(
          "recent_views",
          "campaigns.id",
          "=",
          "recent_views.campaign_id"
        )
        .select("campaigns.*")
        .where(params)
        .andWhere("recent_views.user_id", user.id);
      //   .andWhereIn(
      //   'id',
      //   (query) => query.from('recent_views').select('campaign_id').where('user_id', user.id)
      // )
      if (keyword) {
        keyword = helper.replaceAll(keyword, "_", "\\_");
        keyword = helper.replaceAll(keyword, "%", "\\%");
        campaignQuery.andWhere("name", "like", "%" + keyword + "%");
        // campaignQuery.andWhere((query) => {
        //   query
        //     .where('name', 'like', '%'+keyword+'%')
        //     .orWhere('content', 'like', '%'+keyword+'%')
        // })
      }
      const campaigns = await campaignQuery
        .preload("category")
        .orderBy("recent_views.updated_at", "desc")
        .paginate(page, limit);
      let campaignsData = campaigns.toJSON().data;
      let campaignsMeta = campaigns.toJSON().meta;
      campaignsMeta = helper.formatMeta(campaignsMeta);
      const campaignJson = campaignsData.map(async (campaignsData) => {
        const campaignsDataNew = campaignsData.serialize();
        var favorite = 0;
        let checkCode = await Favorite.query()
          .where({ user_id: user.id, campaign_id: campaignsDataNew.id })
          .first();
        if (!isEmpty(checkCode)) {
          favorite = 1;
        }
        campaignsDataNew.favorite = favorite;
        return campaignsDataNew;
      });
      const campaignJsonNew = await Promise.all(campaignJson);
      Logger.info(
        { campaigns: campaigns.length },
        "Get list campaigns successfully"
      );
      let responseData = {
        success: true,
        message: "success",
        data: campaignJsonNew,
        meta: campaignsMeta,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list campaigns failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api/campaigns/suggested-keywords:
   *   get:
   *     tags:
   *       - Campaigns
   *     summary: Suggested keywords
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: keyword
   *         description: từ khoá tìm kiếm
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                    type: string[]
   */
  public async getSuggestedKeywords({
    request,
    response,
  }: HttpContextContract) {
    try {
      let keyword = request.input("keyword");
      let campaigns: Campaign[] = [];
      let categories: Category[] = [];

      if (keyword) {
        keyword = helper.replaceAll(keyword, "_", "\\_");
        keyword = helper.replaceAll(keyword, "%", "\\%");
        // campaigns = await Campaign.query().where('name', 'like', `%${keyword}%`).exec();
        // categories = await Category.query().where('name', 'like', `%${keyword}%`).exec();
        keyword = keyword.toLowerCase();
        campaigns = await Campaign.query()
          .whereRaw(`LOWER(name) LIKE ?`, [`%${keyword}%`])
          .exec();
        categories = await Category.query()
          .whereRaw(`LOWER(name) LIKE ?`, [`%${keyword}%`])
          .exec();
      }

      return response.status(200).send({
        success: true,
        message: "success",
        data: [
          ...campaigns.map((c) => c.name),
          ...categories.map((c) => c.name),
        ],
      });
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api/campaigns/search-homepage:
   *   get:
   *     tags:
   *       - Campaigns
   *     summary: List Campaign
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: keyword
   *         description: từ khoá tìm kiếm
   *         in: query
   *         required: false
   *       - name: limit
   *         description: limit campaign
   *         in: query
   *         required: false
   *       - name: page
   *         description: page campaign
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Campaign'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async searchHomepage({
    auth,
    request,
    response,
  }: HttpContextContract) {
    try {
      const user = await auth.authenticate();
      let keyword = request.input("keyword");
      let status = CUtilsConfig.status.active; // Chỉ lấy bản ghi active
      let page = request.input("page", 1);
      let limit = request.input("limit", 20);
      let params = {};
      if (status) params["campaigns.status"] = status;
      if (isEmpty(keyword) || typeof keyword === "undefined") {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Keyword not null",
        });
      }
      keyword = keyword.trim();
      keyword = helper.replaceAll(keyword, "_", "\\_");
      keyword = helper.replaceAll(keyword, "%", "\\%");
      keyword = keyword.toLowerCase();
      const campaignQuery = Campaign.query()
        .innerJoin("categories", "campaigns.category_id", "=", "categories.id")
        .select("campaigns.*")
        .where(params)
        .andWhere((query) => {
          query
            // .where('categories.name', 'like', '%'+keyword+'%')
            // .orWhere('campaigns.name', 'like', '%'+keyword+'%')
            .whereRaw(`LOWER(categories.name) LIKE ?`, [`%${keyword}%`])
            .orWhereRaw(`LOWER(campaigns.name) LIKE ?`, [`%${keyword}%`]);
        })
        .orderBy("campaigns.created_at", "desc");

      const campaigns = await campaignQuery
        .preload("category")
        .paginate(page, limit);
      let campaignsData = campaigns.toJSON().data;
      let campaignsMeta = campaigns.toJSON().meta;
      campaignsMeta = helper.formatMeta(campaignsMeta);
      const campaignJson = campaignsData.map(async (campaignsData) => {
        const campaignsDataNew = campaignsData.serialize();
        var favorite = 0;
        let checkCode = await Favorite.query()
          .where({ user_id: user.id, campaign_id: campaignsDataNew.id })
          .first();
        if (!isEmpty(checkCode)) {
          favorite = 1;
        }
        campaignsDataNew.favorite = favorite;
        if (campaignsDataNew.images) {
          campaignsDataNew.images = JSON.parse(campaignsDataNew.images);
        }
        if (campaignsDataNew.options) {
          campaignsDataNew.options = JSON.parse(campaignsDataNew.options);
        }
        if (campaignsDataNew.benefits_tag) {
          campaignsDataNew.benefits_tag = JSON.parse(
            campaignsDataNew.benefits_tag
          );
        }
        return campaignsDataNew;
      });
      const campaignJsonNew = await Promise.all(campaignJson);
      Logger.info(
        { campaigns: campaigns.length },
        "Get list campaigns successfully"
      );
      let responseData = {
        success: true,
        message: "success",
        data: campaignJsonNew,
        meta: campaignsMeta,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list campaigns failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }
}
