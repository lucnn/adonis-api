import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Admin from "App/Models/Admin";

export default class UsersCmsController {

  /**
   * @swagger
   * /api-cms/users/info:
   *   get:
   *     tags:
   *       - CMS User
   *     summary: Info User
   *     security:
   *       - bearerAuth: []
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/AdminInfo'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async info({ auth, response }: HttpContextContract) {
    try {
      const userInfo = await Admin.find(auth.use("apiAdmin").user!.id);
      let responseData = {
        success: true,
        message: 'success',
        data: {...userInfo?.toJSON()}
      }
      return response.json(responseData)
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }
}
