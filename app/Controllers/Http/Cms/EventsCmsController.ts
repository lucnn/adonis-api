import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Event from "App/Models/Event";
import Logger from "@ioc:Adonis/Core/Logger";
import EventValidator from 'App/Validators/EventValidator';
import CUtilsConfig from "Config/CUtilsConfig";
const Help = require("App/Common/Help");
let helper = new Help()

export default class EventsCmsController {
  /**
  * @swagger
  * /api-cms/events:
  *   get:
  *     tags:
  *       - CMS Events
  *     summary: List Event
  *     security:
  *       - bearerAuth: []
  *     parameters:
  *       - name: keyword
  *         description: từ khoá tìm kiếm
  *         in: query
  *         required: false
  *       - name: order
  *         description: order sort
  *         in: query
  *         required: false
  *       - name: type
  *         description: type event
  *         in: query
  *         required: false
  *       - name: position
  *         description: position event
  *         in: query
  *         required: false
  *       - name: status
  *         description: status event
  *         in: query
  *         required: false
   *       - name: limit
   *         description: limit
   *         in: query
   *         required: false
   *       - name: page
   *         description: page
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Event'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async index({ request, response }: HttpContextContract) {
    try {
      let keyword = request.input("keyword")
      let order = request.input("order")
      let type = request.input('type')
      let position = request.input('position')
      let status = request.input("status")
      let page = request.input('page', 1)
      let limit = request.input('limit', 20)
      let params = {}
      if (order) params["order"] = order
      if (type) params["type"] = type
      if (position) params["position"] = position
      if (status) params["status"] = status
      const eventQuery = Event.query().where(params)
        if(keyword){
          keyword = helper.replaceAll(keyword, '_','\\_')
          keyword = helper.replaceAll(keyword, '%','\\%')
          eventQuery.andWhere('title', 'like', '%'+keyword+'%')
          // eventQuery.andWhere((query) => {
          //   query
          //     .where('title', 'like', '%'+keyword+'%')
          //     .orWhere('content', 'like', '%'+keyword+'%')
          // })
        }
      const events = await eventQuery.orderBy('created_at', 'desc').paginate(page, limit);
      let eventsData = events.toJSON().data
      let eventsMeta = events.toJSON().meta
      eventsMeta = helper.formatMeta(eventsMeta)
      const eventJson = eventsData.map((events) => events.serialize())
      Logger.info({ events: events.length }, "Get list events successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: eventJson,
        meta: eventsMeta
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list events failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api-cms/events/{id}:
   *   get:
   *     tags:
   *       - CMS Events
   *     summary: Detail Event
   *     security:
   *       - bearerAuth: []
  *     parameters:
  *       - name: id
  *         description: id Event
  *         in: path
  *         required: true
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Event'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async show({ request, response }: HttpContextContract) {
    try {
      let id = request.param("id")
      const event = await Event.find(id);
      if (!event) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Event not found",
          messageKey: "EVENT_NOT_FOUND",
          data: null
        })
      }
      let responseData = {
        success: true,
        message: 'success',
        data: event ? event?.toJSON() : null
      }
      Logger.info({ event: 1 }, "Get event successfully");
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get event failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

/**
  * @swagger
  * /api-cms/events/{id}:
  *   put:
  *     tags:
  *       - CMS Events
  *     summary: Update Event
  *     security:
  *       - bearerAuth: []
  *     parameters:
  *       - name: id
  *         description: id event
  *         in: path
  *         required: true
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/definitions/EventForm'
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Event'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async update({request, response }: HttpContextContract) {
    try {
      await request.validate(EventValidator)
      const timeCurrent = Math.floor(Date.now() / 1000);
      const event = await Event.find(request.param('id'));
      let responseData = {
        success: true,
        message: 'success',
        data: []
      }
      if (event) {
        event.title = request.input("title");
        event.image = request.input("image");
        event.content = request.input("content");
        event.status = request.input("status");
        event.durationStart = request.input("durationStart");
        event.durationEnd = request.input("durationEnd");
        if(event.durationStart < timeCurrent){
          event.status = CUtilsConfig.status.active
        }
        if(event.durationEnd < timeCurrent){
          event.status = CUtilsConfig.status.cancel
        }
        if(event.durationStart > timeCurrent){
          event.status = CUtilsConfig.status.warning
        }
        if (await event.save()) {
          // @ts-ignore
          responseData.data = event.serialize()
          return response.json(responseData)
        } else {
          responseData.success = false
          responseData.message = "Update event fail"
          return response.status(422).send(responseData)
        }
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Update event failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

/**
  * @swagger
  * /api-cms/events:
  *   post:
  *     tags:
  *       - CMS Events
  *     summary: Create Event
  *     security:
  *       - bearerAuth: []
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/definitions/EventForm'
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Event'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async store({ response, request }: HttpContextContract) {
    try {
      const timeCurrent = Math.floor(Date.now() / 1000);
      await request.validate(EventValidator)
      const event = new Event();
      event.title = request.input("title");
      event.image = request.input("image");
      event.content = request.input("content");
      event.status = request.input("status");
      event.durationStart = request.input("durationStart");
      event.durationEnd = request.input("durationEnd");
      if(event.durationStart < timeCurrent){
        event.status = CUtilsConfig.status.active
      }
      if(event.durationEnd < timeCurrent){
        event.status = CUtilsConfig.status.cancel
      }
      if(event.durationStart > timeCurrent){
        event.status = CUtilsConfig.status.warning
      }
      await event.save();
      let eventJson = event.serialize()
      Logger.info({ event: event.id }, "Create event successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: eventJson
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Create event failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api-cms/events/{id}:
   *   delete:
   *     tags:
   *       - CMS Events
   *     summary: Delete Event
   *     security:
   *       - bearerAuth: []
   *     parameters:
  *       - name: id
  *         description: id event
  *         in: path
  *         required: true
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async destroy({response,request}: HttpContextContract) {
    try {
       let event = await Event.query()
      .where("id", request.param('id'))
      .delete();
      if (!event[0]) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Event not found",
          messageKey: "EVENT_NOT_FOUND",
          data: null
        })
      }
      // return response.redirect("/dashboard");
      return response.status(200).send({
        success: true,
        message: "Success",
        data: null
      })
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Delete event failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }
}
