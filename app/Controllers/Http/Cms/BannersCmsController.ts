import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Banner from "App/Models/Banner";
import Logger from "@ioc:Adonis/Core/Logger";
import CUtilsConfig from "Config/CUtilsConfig";
import BannerValidator from "App/Validators/BannerValidator";
const Help = require("App/Common/Help");
let helper = new Help()

export default class BannersCmsController {

  /**
  * @swagger
  * /api-cms/banners:
  *   get:
  *     tags:
  *       - CMS Banners
  *     summary: List Banner
  *     security:
  *       - bearerAuth: []
  *     parameters:
  *       - name: keyword
  *         description: từ khoá tìm kiếm
  *         in: query
  *         required: false
  *       - name: order
  *         description: order sort
  *         in: query
  *         required: false
  *       - name: type
  *         description: type banner
  *         in: query
  *         required: false
  *       - name: position
  *         description: position banner
  *         in: query
  *         required: false
  *       - name: page
  *         description: page banner
  *         in: query
  *         required: false
  *       - name: limit
  *         description: limit banner
  *         in: query
  *         required: false
  *       - name: status
  *         description: status banner
  *         in: query
  *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Banner'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async index({ request, response }: HttpContextContract) {
    try {
      let keyword = request.input("keyword")
      let order = request.input("order")
      let type = request.input('type')
      let position = request.input('position')
      let page = request.input('page', 1)
      let limit = request.input('limit', 20)
      let status = request.input("status")
      let params = {}
      if (order) params["order"] = order
      if (type) params["type"] = type
      if (position) params["position"] = position
      if (status) params['status'] = status
      const bannerQuery = Banner.query().where(params)
      if(keyword){
        keyword = helper.replaceAll(keyword, '_','\\_')
        keyword = helper.replaceAll(keyword, '%','\\%')
        bannerQuery.where('name', 'like', '%'+keyword+'%')
      }
      const banners = await bannerQuery.orderBy('created_at', 'desc').preload("event").paginate(page, limit);
      let bannersData = banners.toJSON().data
      let bannersMeta = banners.toJSON().meta
      bannersMeta = helper.formatMeta(bannersMeta)
      const bannerJson = bannersData.map((bannersData) => bannersData.serialize())
      Logger.info({ banners: bannersData.length }, "Get list banners successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: bannerJson,
        meta: bannersMeta
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list banners failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api-cms/banners/{id}:
   *   get:
   *     tags:
   *       - CMS Banners
   *     summary: Detail Banner
   *     security:
   *       - bearerAuth: []
  *     parameters:
  *       - name: id
  *         description: id Banner
  *         in: path
  *         required: true
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Banner'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async show({ request, response }: HttpContextContract) {
    try {
      let id = request.param("id")
      const banner = await Banner.find(id);
      if (!banner) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Banner not found",
          messageKey: "BANNER_NOT_FOUND",
          data: null
        })
      }
      await banner.preload("event")

      let responseData = {
        success: true,
        message: 'success',
        data: banner ? banner?.toJSON() : null
      }
      Logger.info({ banner: 1 }, "Get banner successfully");
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get banner failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

/**
  * @swagger
  * /api-cms/banners/{id}:
  *   put:
  *     tags:
  *       - CMS Banners
  *     summary: Update Banner
  *     security:
  *       - bearerAuth: []
  *     parameters:
  *       - name: id
  *         description: id banner
  *         in: path
  *         required: true
  *     requestBody:
  *      required: true
  *      content:
  *        application/json:
  *          schema:
  *            properties:
  *              name:
  *                description: name banner
  *                type: string
  *                required: true
  *              path:
  *                description: path banner
  *                type: string
  *                required: true
  *              order:
  *                description: order banner
  *                type: integer
  *                required: false
  *              durationStart:
  *                description: duration start banner
  *                type: integer
  *                required: false
  *              durationEnd:
  *                description: duration end banner
  *                type: integer
  *                required: false
  *              eventId:
  *                description: eventId
  *                type: integer
  *                required: false
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Banner'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async update({request, response }: HttpContextContract) {
    try {
      await request.validate(BannerValidator)
      const timeCurrent = Math.floor(Date.now() / 1000);
      const banner = await Banner.find(request.param('id'));
      let responseData = {
        success: true,
        message: 'success',
        data: []
      }
      if (banner) {
        banner.name = request.input("name");
        banner.path = request.input("path");
        banner.order = request.input("order");
        banner.durationStart = request.input("durationStart");
        banner.durationEnd = request.input("durationEnd");
        if(banner.durationStart < timeCurrent){
          banner.status = CUtilsConfig.status.active
        }
        if(banner.durationEnd < timeCurrent){
          banner.status = CUtilsConfig.status.cancel
        }
        if(banner.durationStart > timeCurrent){
          banner.status = CUtilsConfig.status.warning
        }
        banner.eventId = request.input("eventId");
        if (await banner.save()) {
          // @ts-ignore
          responseData.data = banner.serialize()
          return response.json(responseData)
        } else {
          responseData.success = false
          responseData.message = "Update banner fail"
          return response.status(422).send(responseData)
        }
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Update banner failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

/**
  * @swagger
  * /api-cms/banners:
  *   post:
  *     tags:
  *       - CMS Banners
  *     summary: Create Banner
  *     security:
  *       - bearerAuth: []
  *     requestBody:
  *      required: true
  *      content:
  *        application/json:
  *          schema:
  *            properties:
  *              name:
  *                description: name banner
  *                type: string
  *                required: true
  *              path:
  *                description: path in banner
  *                type: string
  *                required: false
  *              order :
  *                description: order  banner
  *                type: integer
  *                required: false
 *              durationStart:
 *                description: duration start banner
 *                type: integer
 *                required: false
 *              durationEnd:
 *                description: duration end banner
 *                type: integer
 *                required: false
 *              eventId:
 *                description: eventId
 *                type: integer
 *                required: false
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Banner'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async store({ response, request }: HttpContextContract) {
    try {
      await request.validate(BannerValidator)
      const timeCurrent = Math.floor(Date.now() / 1000);
      const banner = new Banner();
      banner.name = request.input("name");
      banner.path = request.input("path");
      banner.order = request.input("order");
      banner.durationStart = request.input("durationStart");
      banner.durationEnd = request.input("durationEnd");
      if(banner.durationStart < timeCurrent){
        banner.status = CUtilsConfig.status.active
      }
      if(banner.durationEnd < timeCurrent){
        banner.status = CUtilsConfig.status.cancel
      }
      if(banner.durationStart > timeCurrent){
        banner.status = CUtilsConfig.status.warning
      }
      banner.eventId = request.input("eventId");
      await banner.save();
      let bannerJson = banner.serialize()
      Logger.info({ banner: banner.id }, "Create banner successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: bannerJson
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Create banner failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api-cms/banners/{id}:
   *   delete:
   *     tags:
   *       - CMS Banners
   *     summary: Delete Banner
   *     security:
   *       - bearerAuth: []
   *     parameters:
  *       - name: id
  *         description: id banner
  *         in: path
  *         required: true
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async destroy({response,request}: HttpContextContract) {
    try {
       let banner = await Banner.query()
      .where("id", request.param('id'))
      .delete();
      if (!banner[0]) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Banner not found",
          messageKey: "BANNER_NOT_FOUND",
          data: null
        })
      }
      // return response.redirect("/dashboard");
      return response.status(200).send({
        success: true,
        message: "Success",
        data: null
      })
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Delete banner failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }
}
