import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Logger from "@ioc:Adonis/Core/Logger";
import Deal from "App/Models/Deal";
import Order from "App/Models/Order";
import PointHistory from "App/Models/PointHistory";
import UpdateOrderValidator from "App/Validators/UpdateOrderValidator";
import CUtilsConfig from "Config/CUtilsConfig";
import omit from "lodash/omit";
import pick from "lodash/pick";
import { DateTime } from "luxon";
const Help = require("App/Common/Help");
let helper = new Help();
const filterKeys = ["orders.id", "users.username", "campaigns.name"];

export default class OrdersCmsController {
  public async index({ request, response }: HttpContextContract) {
    try {
      let keyword = request.input("keyword");
      let filterKey = request.input("filterKey", filterKeys[2]);
      let page = request.input("page", 1);
      let limit = request.input("limit", 20);
      let status = +request.input("status");
      let params = {};
      const query = Order.query()
        .preload("user")
        .preload("campaign")
        .preload("deal")
        .innerJoin("users", "orders.user_id", "=", "users.id")
        .leftJoin("deals", "orders.deal_id", "=", "deals.id")
        .innerJoin("campaigns", "orders.campaign_id", "=", "campaigns.id")
        .select("orders.*")
        .whereNull("deleted_at")
        .where("orders.status", ">", CUtilsConfig.orderStatus.initiated);
      if (status) {
        console.log(CUtilsConfig.orderStatus.pending === status);

        switch (status) {
          case CUtilsConfig.orderStatus.pending:
            query.whereNull("orders.deal_id");
            break;
          case CUtilsConfig.orderStatus.unmatched:
            query.where("deals.status", "=", CUtilsConfig.dealStatus.active);
            status = CUtilsConfig.orderStatus.pending;
            break;
          case CUtilsConfig.orderStatus.matched:
            query.where("deals.status", "=", CUtilsConfig.dealStatus.matched);
            status = CUtilsConfig.orderStatus.pending;
            break;
          default:
            break;
        }
        params["orders.status"] = status;
      }
      query.where(params);
      if (keyword && filterKeys.includes(filterKey)) {
        query.where(filterKey, "like", "%" + keyword + "%");
      }
      const orders = await query
        .orderBy("created_at", "desc")
        .paginate(page, limit);
      let ordersData = orders.toJSON().data;
      let ordersMeta = orders.toJSON().meta;
      ordersMeta = helper.formatMeta(ordersMeta);
      const orderJson = ordersData.map((ordersData) => {
        const data = ordersData.serialize();
        if (
          ordersData.status === CUtilsConfig.orderStatus.pending &&
          ordersData.deal
        ) {
          data.status =
            ordersData.deal.status === CUtilsConfig.dealStatus.matched
              ? CUtilsConfig.orderStatus.matched
              : CUtilsConfig.orderStatus.unmatched;
        }
        return {
          ...omit(data, ["deal"]),
          user: pick(data.user, ["username"]),
          campaign: pick(data.campaign, ["name", "thumbnail"]),
        };
      });
      Logger.info(
        { orders: ordersData.length },
        "Get list orders successfully"
      );
      let responseData = {
        success: true,
        message: "success",
        data: orderJson,
        meta: ordersMeta,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list orders failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  public async show({ request, response }: HttpContextContract) {
    try {
      const order = await Order.find(request.param("id"));
      if (!order) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Order not found",
          messageKey: "ORDER_NOT_FOUND",
          data: null,
        });
      }
      let data: any = null;
      if (order) {
        await order.preload("userAddress");
        await order.preload("campaign");
        await order.preload("user");
        await order.preload("deal");
        data = order.toJSON();
        let pairedOrder: any = null;
        if (order.deal?.status === CUtilsConfig.dealStatus.matched) {
          pairedOrder = await Order.query()
            .where({ deal_id: order.dealId })
            .whereNot("id", order.id)
            .where("status", ">", CUtilsConfig.orderStatus.initiated)
            .first();
          if (pairedOrder) {
            pairedOrder = {
              id: pairedOrder.id,
            };
          }
        }
        if (data.status === CUtilsConfig.orderStatus.pending && data.deal) {
          data.status =
            data.deal.status === CUtilsConfig.dealStatus.matched
              ? CUtilsConfig.orderStatus.matched
              : CUtilsConfig.orderStatus.unmatched;
        }
        delete data.deal;
        data = {
          ...data,
          user: pick(data.user, ["id", "name", "username"]),
          options: JSON.parse(order.options),
          pairedOrder,
        };
      }
      let responseData = {
        success: true,
        message: "success",
        data,
      };
      Logger.info({ order: 1 }, "Get order successfully");
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get order failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  public async update({ request, response }: HttpContextContract) {
    try {
      await request.validate(UpdateOrderValidator);
      const order = await Order.find(request.param("id"));
      let responseData = {
        success: true,
        message: "success",
        data: null as any,
      };
      if (order) {
        order.shippingDate = request.input("shippingDate");
        order.trackingNumber = request.input("trackingNumber");
        let status = request.input("status");
        if (
          [
            CUtilsConfig.orderStatus.unmatched,
            CUtilsConfig.orderStatus.matched,
          ].includes(status)
        ) {
          status = CUtilsConfig.orderStatus.pending;
        }
        const deal = await Deal.query()
          .where({
            id: order.dealId,
            status: CUtilsConfig.dealStatus.matched,
          })
          .first();
        if (
          order.status === CUtilsConfig.orderStatus.pending &&
          status === CUtilsConfig.orderStatus.packing &&
          (Boolean(deal) || !order.dealId)
        ) {
          order.status = status;
        } else if (
          order.status === CUtilsConfig.orderStatus.packing &&
          status === CUtilsConfig.orderStatus.onDelivery
        ) {
          order.status = status;
        } else if (
          order.status === CUtilsConfig.orderStatus.onDelivery &&
          status === CUtilsConfig.orderStatus.completed
        ) {
          order.status = status;
          if (deal?.makerId === order.userId) {
            await order.preload("user");
            const pointHistory = new PointHistory();
            pointHistory.userId = order.user.id;
            pointHistory.value = Math.round(order.grandTotal * 0.01);
            pointHistory.type = CUtilsConfig.pointHistoryType.openDealIncentive;
            order.user.point += pointHistory.value;
            await order.user.save();
            await pointHistory.save();
          }
        } else if (
          order.status === CUtilsConfig.orderStatus.refundProcess &&
          status === CUtilsConfig.orderStatus.refunded
        ) {
          order.status = status;
        }
        if (await order.save()) {
          responseData.data = order.serialize();
          return response.json(responseData);
        } else {
          responseData.success = false;
          responseData.message = "Update order fail";
          return response.status(422).send(responseData);
        }
      }
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Update order failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  public async destroy({ response, request }: HttpContextContract) {
    try {
      const order = await Order.find(request.param("id"));
      if (!order) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Order not found",
          data: null,
        });
      }
      if (
        ![
          CUtilsConfig.orderStatus.cancelled,
          CUtilsConfig.orderStatus.completed,
          CUtilsConfig.orderStatus.refunded,
        ].includes(order.status)
      ) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Cannot delete order",
          data: null,
        });
      }
      order.deletedAt = DateTime.local();
      await order.save();
      return response.status(200).send({
        success: true,
        message: "Success",
        data: null,
      });
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Delete order failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }
}
