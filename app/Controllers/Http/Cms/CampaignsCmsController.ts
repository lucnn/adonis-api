import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Campaign from "App/Models/Campaign";
import Logger from "@ioc:Adonis/Core/Logger";
import Favorite from "App/Models/Favorite";
import CampaignValidator from "App/Validators/CampaignValidator";
import CUtilsConfig from "Config/CUtilsConfig";
import Deal from "App/Models/Deal";
import { DateTime } from "luxon";
const isEmpty = require("lodash.isempty");
const Help = require("App/Common/Help");
let helper = new Help();

export default class CampaignsCmsController {
  /**
   * @swagger
   * /api-cms/campaigns:
   *   get:
   *     tags:
   *       - CMS Campaigns
   *     summary: List Campaign
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: keyword
   *         description: từ khoá tìm kiếm
   *         in: query
   *         required: false
   *       - name: megaDeal
   *         description: mega deal
   *         in: query
   *         required: false
   *       - name: categoryId
   *         description: category id campaign
   *         in: query
   *         required: false
   *       - name: status
   *         description: status campaign
   *         in: query
   *         required: false
   *       - name: limit
   *         description: limit campaign
   *         in: query
   *         required: false
   *       - name: page
   *         description: page campaign
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Campaign'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async index({ request, response }: HttpContextContract) {
    try {
      let keyword = request.input("keyword");
      let categoryId = request.input("categoryId");
      let megaDeal = request.input("megaDeal");
      let status = request.input("status");
      let page = request.input("page", 1);
      let limit = request.input("limit", 20);
      let params = {};
      if (categoryId) params["category_id"] = categoryId;
      if (megaDeal) params["mega_deal"] = megaDeal;
      if (status) params["status"] = status;
      const campaignQuery = Campaign.query().where(params);
      if (keyword) {
        keyword = helper.replaceAll(keyword, "_", "\\_");
        keyword = helper.replaceAll(keyword, "%", "\\%");
        campaignQuery.andWhere("name", "like", "%" + keyword + "%");
        // campaignQuery.andWhere((query) => {
        //   query
        //     .where('name', 'like', '%'+keyword+'%')
        //     .orWhere('content', 'like', '%'+keyword+'%')
        // })
      }
      const campaigns = await campaignQuery
        .preload("category")
        .orderBy("created_at", "desc")
        .paginate(page, limit);
      let campaignsData = campaigns.toJSON().data;
      let campaignsMeta = campaigns.toJSON().meta;
      campaignsMeta = helper.formatMeta(campaignsMeta);
      const campaignJson = campaignsData.map(async (campaignsData) => {
        const campaignsDataNew = campaignsData.serialize();
        if (campaignsDataNew.images) {
          campaignsDataNew.images = JSON.parse(campaignsDataNew.images);
        }
        // if(campaignsDataNew.options){
        //   campaignsDataNew.options = JSON.parse(campaignsDataNew.options)
        // }
        if (campaignsDataNew.benefits_tag) {
          campaignsDataNew.benefits_tag = JSON.parse(
            campaignsDataNew.benefits_tag
          );
        }
        if (campaignsDataNew.mega_deal) {
          const count = await Deal.query()
            .where("campaign_id", campaignsData.id)
            .where(
              "created_at",
              ">",
              DateTime.fromSeconds(campaignsData.megaDealDurationStart).toISO()
            )
            .where(
              "created_at",
              "<",
              DateTime.fromSeconds(campaignsData.megaDealDurationEnd).toISO()
            )
            .where("status", CUtilsConfig.status.cancel)
            .count("id");
          campaignsDataNew.participation_rate = count[0]["count(`id`)"] * 2;
          campaignsDataNew.accomplishment_rate =
            campaignsDataNew.target_number_people;
        }
        return campaignsDataNew;
      });
      Logger.info(
        { campaigns: campaigns.length },
        "Get list campaigns successfully"
      );
      let responseData = {
        success: true,
        message: "success",
        data: await Promise.all(campaignJson),
        meta: campaignsMeta,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list campaigns failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api-cms/campaigns/{id}:
   *   get:
   *     tags:
   *       - CMS Campaigns
   *     summary: Detail Campaign
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: id
   *         description: id Campaign
   *         in: path
   *         required: true
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Campaign'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async show({ request, response }: HttpContextContract) {
    try {
      let id = request.param("id");
      const campaign = await Campaign.find(id);
      if (campaign) {
        await campaign.preload("category");
      }
      if (!campaign) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Campaign not found",
          messageKey: "CAMPAIGN_NOT_FOUND",
          data: null,
        });
      }
      const campaignsData = campaign?.serialize();
      if (campaignsData.images) {
        campaignsData.images = JSON.parse(campaignsData.images);
      }
      // if(campaignsData.options){
      //   campaignsData.options = JSON.parse(campaignsData.options)
      // }
      if (campaignsData.benefits_tag) {
        campaignsData.benefits_tag = JSON.parse(campaignsData.benefits_tag);
      }
      let responseData = {
        success: true,
        message: "success",
        data: campaignsData,
      };
      Logger.info({ campaign: 1 }, "Get campaign successfully");
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get campaign failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api-cms/campaigns/{id}:
   *   put:
   *     tags:
   *       - CMS Campaigns
   *     summary: Update Campaign
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: id
   *         description: id campaign
   *         in: path
   *         required: true
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/definitions/CampaignForm'
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Campaign'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async update({ request, response }: HttpContextContract) {
    try {
      await request.validate(CampaignValidator);
      const campaign = await Campaign.find(request.param("id"));
      let responseData = {
        success: true,
        message: "success",
        data: [],
      };
      if (campaign) {
        const timeCurrent = Math.floor(Date.now() / 1000);
        campaign.name = request.input("name");
        campaign.brand = request.input("brand");
        campaign.content = request.input("content");
        campaign.thumbnail = request.input("thumbnail");
        if (typeof request.input("images") == "object") {
          campaign.images =
            JSON.stringify(request.input("images")).toString() + "";
        }
        if (typeof request.input("options") == "object") {
          campaign.options =
            JSON.stringify(request.input("options")).toString() + "";
        }
        if (typeof request.input("benefitsTag") == "object") {
          campaign.benefitsTag =
            JSON.stringify(request.input("benefitsTag")).toString() + "";
        }
        campaign.categoryId = request.input("categoryId");
        campaign.dealDurationStart = request.input("dealDurationStart");
        campaign.dealDurationEnd = request.input("dealDurationEnd");
        campaign.originalPrice = request.input("originalPrice");
        campaign.discountPrice = request.input("discountPrice");
        campaign.dealPrice = request.input("dealPrice");
        campaign.megaDeal = request.input("megaDeal");
        campaign.extraDiscountRate = request.input("extraDiscountRate");
        campaign.refundPoints = request.input("refundPoints");
        campaign.targetNumberPeople = request.input("targetNumberPeople");
        campaign.megaDealDurationStart = request.input("megaDealDurationStart");
        campaign.megaDealDurationEnd = request.input("megaDealDurationEnd");
        // if(campaign.megaDeal == CUtilsConfig.megaDeal.warning){
        if (campaign.dealDurationStart < timeCurrent) {
          campaign.status = CUtilsConfig.status.active;
        }
        if (campaign.dealDurationEnd < timeCurrent) {
          campaign.status = CUtilsConfig.status.cancel;
        }
        if (campaign.dealDurationStart > timeCurrent) {
          campaign.status = CUtilsConfig.status.warning;
        }
        // }else{
        //   if(campaign.megaDealDurationStart < timeCurrent){
        //     campaign.status = CUtilsConfig.status.active
        //   }
        //   if(campaign.megaDealDurationEnd < timeCurrent){
        //     campaign.status = CUtilsConfig.status.cancel
        //   }
        //   if(campaign.megaDealDurationStart > timeCurrent){
        //     campaign.status = CUtilsConfig.status.warning
        //   }
        // }
        campaign.freeShipping = request.input("freeShipping", 1);
        campaign.shippingFee = request.input("shippingFee");
        if (await campaign.save()) {
          const campaignsData = campaign.serialize();
          if (campaignsData.images) {
            campaignsData.images = JSON.parse(campaignsData.images);
          }
          if (campaignsData.options) {
            campaignsData.options = JSON.parse(campaignsData.options);
          }
          if (campaignsData.benefits_tag) {
            campaignsData.benefits_tag = JSON.parse(campaignsData.benefits_tag);
          }
          // @ts-ignore
          responseData.data = campaignsData;
          return response.json(responseData);
        } else {
          responseData.success = false;
          responseData.message = "Update campaign fail";
          return response.status(422).send(responseData);
        }
      }
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Update campaign failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api-cms/campaigns:
   *   post:
   *     tags:
   *       - CMS Campaigns
   *     summary: Create Campaign
   *     security:
   *       - bearerAuth: []
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/definitions/CampaignForm'
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Campaign'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async store({ auth, response, request }: HttpContextContract) {
    try {
      await request.validate(CampaignValidator);
      const timeCurrent = Math.floor(Date.now() / 1000);
      const campaign = new Campaign();
      campaign.name = request.input("name");
      campaign.brand = request.input("brand");
      campaign.content = request.input("content");
      campaign.thumbnail = request.input("thumbnail");
      campaign.status = request.input("status");
      campaign.adminId = auth.use("apiAdmin").user!.id;
      if (typeof request.input("images") == "object") {
        campaign.images =
          JSON.stringify(request.input("images")).toString() + "";
      }
      if (typeof request.input("options") == "object") {
        campaign.options =
          JSON.stringify(request.input("options")).toString() + "";
      }
      if (typeof request.input("benefitsTag") == "object") {
        campaign.benefitsTag =
          JSON.stringify(request.input("benefitsTag")).toString() + "";
      }
      campaign.categoryId = request.input("categoryId");
      campaign.dealDurationStart = request.input("dealDurationStart");
      campaign.dealDurationEnd = request.input("dealDurationEnd");
      campaign.originalPrice = request.input("originalPrice");
      campaign.discountPrice = request.input("discountPrice");
      campaign.dealPrice = request.input("dealPrice");
      campaign.megaDeal = request.input("megaDeal");
      campaign.extraDiscountRate = request.input("extraDiscountRate");
      campaign.refundPoints = request.input("refundPoints");
      campaign.targetNumberPeople = request.input("targetNumberPeople");
      campaign.megaDealDurationStart = request.input("megaDealDurationStart");
      campaign.megaDealDurationEnd = request.input("megaDealDurationEnd");
      // if(campaign.megaDeal == CUtilsConfig.megaDeal.warning){
      if (campaign.dealDurationStart < timeCurrent) {
        campaign.status = CUtilsConfig.status.active;
      }
      if (campaign.dealDurationEnd < timeCurrent) {
        campaign.status = CUtilsConfig.status.cancel;
      }
      if (campaign.dealDurationStart > timeCurrent) {
        campaign.status = CUtilsConfig.status.warning;
      }
      // }else{
      //   if(campaign.megaDealDurationStart < timeCurrent){
      //     campaign.status = CUtilsConfig.status.active
      //   }
      //   if(campaign.megaDealDurationEnd < timeCurrent){
      //     campaign.status = CUtilsConfig.status.cancel
      //   }
      //   if(campaign.megaDealDurationStart > timeCurrent){
      //     campaign.status = CUtilsConfig.status.warning
      //   }
      // }
      campaign.freeShipping = request.input("freeShipping", 1);
      campaign.shippingFee = request.input("shippingFee");
      await campaign.save();
      let campaignJson = campaign.serialize();
      if (campaignJson.images) {
        campaignJson.images = JSON.parse(campaignJson.images);
      }
      if (campaignJson.options) {
        campaignJson.options = JSON.parse(campaignJson.options);
      }
      if (campaignJson.benefits_tag) {
        campaignJson.benefits_tag = JSON.parse(campaignJson.benefits_tag);
      }
      Logger.info({ campaign: campaign.id }, "Create campaign successfully");
      let responseData = {
        success: true,
        message: "success",
        data: campaignJson,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Create campaign failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api-cms/campaigns/{id}:
   *   delete:
   *     tags:
   *       - CMS Campaigns
   *     summary: Delete Campaign
   *     security:
   *       - bearerAuth: []
   *     parameters:
   *       - name: id
   *         description: id campaign
   *         in: path
   *         required: true
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async destroy({ response, request }: HttpContextContract) {
    try {
      let campaign = await Campaign.query()
        .where("id", request.param("id"))
        .delete();
      if (!campaign[0]) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Campaign not found",
          messageKey: "CAMPAIGN_NOT_FOUND",
          data: null,
        });
      }
      // return response.redirect("/dashboard");
      return response.status(200).send({
        success: true,
        message: "Success",
        data: null,
      });
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Delete campaign failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  /**
   * @swagger
   * /api-cms/campaigns/favorite:
   *   post:
   *     tags:
   *       - CMS Campaigns
   *     summary: favorite Campaign
   *     security:
   *       - bearerAuth: []
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/definitions/CampaignFavorite'
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async favorite({ auth, response, request }: HttpContextContract) {
    try {
      const user = await auth.authenticate();
      let campaign_id = request.input("campaignId");
      let status = request.input("status", 0);
      let favorite = await Favorite.query()
        .where({ user_id: user.id, campaign_id: campaign_id })
        .first();
      if (isEmpty(favorite)) {
        if (status == 1) {
          const favorite = new Favorite();
          favorite.userId = user.id;
          favorite.campaignId = campaign_id;
          favorite.save();
        }
      } else {
        if (status == 0) {
          await Favorite.query().where("id", favorite!.id).delete();
        }
      }
      Logger.info({ campaign: campaign_id }, "favorite successfully");
      let responseData = {
        success: true,
        message: "success",
        data: null,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "favorite failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }
}
