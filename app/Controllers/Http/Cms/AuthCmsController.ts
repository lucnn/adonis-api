import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Admin from "App/Models/Admin";
import Logger from "@ioc:Adonis/Core/Logger";
import RegisterAdminValidator from 'App/Validators/RegisterAdminValidator';
import LoginValidator from 'App/Validators/LoginValidator';
import CUtilsConfig from "Config/CUtilsConfig";

export default class AuthCmsController {
  /**
   * @swagger
   * /api-cms/login:
   *   post:
   *     tags:
   *       - CMS Auth
   *     summary: Login API
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/definitions/AuthLogin'
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/AdminInfo'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async login({ request, auth, response }: HttpContextContract) {
    try {
      await request.validate(LoginValidator)
      const params = request.body();

      const tokenInfo = await auth.use("apiAdmin").attempt(params.username, params.password, {
        expiresIn: CUtilsConfig.token.expiresIn,
      });
      Logger.info({ user: auth.use("apiAdmin").user!.id }, "User login successfully");
      const userInfo = await Admin.find(auth.use("apiAdmin").user!.id);
      let responseData = {
        success: true,
        message: 'success',
          data: {...userInfo?.toJSON(), token: tokenInfo.token, tokenExpiresAt: tokenInfo.expiresAt!.toString()}
      }
      return response.json(responseData)
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api-cms/register:
   *   post:
   *     tags:
   *       - CMS Auth
   *     summary: Register API
   *     requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/definitions/AdminRegister'
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/AdminInfo'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
   public async register({ request, auth, response }: HttpContextContract) {
    try {
      await request.validate(RegisterAdminValidator)

      const params = request.body();
      /**
       * Create a new user
       */
      delete params.passwordConfirm
      const user = await Admin.create(params);
      Logger.info({ user: user.id }, "User register successfully");

      const tokenInfo = await auth.use("apiAdmin").attempt(params.username, params.password, {
        expiresIn: CUtilsConfig.token.expiresIn,
      });

      Logger.info({ user: user.id }, "User login successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: {...user?.toJSON(), token: tokenInfo.token, tokenExpiresAt: tokenInfo.expiresAt!.toString()}
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "User register failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }
}
