import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Application from '@ioc:Adonis/Core/Application'

export default class FilesCmsController {
  /**
   * @swagger
   * /api-cms/files/upload:
   *   post:
   *     tags:
   *       - CMS File
   *     summary: Upload File
   *     security:
   *       - bearerAuth: []
   *     requestBody:
   *      required: true
   *      content:
   *        multipart/form-data:
   *          schema:
   *            type: object
   *            properties:
   *              file:
   *                type: string
   *                format: binary
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/PathFile'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async upload({ request,response }: HttpContextContract) {
    try {
      const file = request.file('file')
      const path = 'uploads'
      let filelocation;
      if (file) {
        // let file_name = `${new Date().getTime()}-`+file.clientName+'.'+file.extname;
        let file_name = `${new Date().getTime()}-`+file.clientName;
        await file.move(Application.publicPath(path), {
          name: file_name,
          overwrite: true, // overwrite in case of conflict
        })
        filelocation = path+ '/' + file_name
      }
      let responseData = {
        success: true,
        message: 'success',
        data: {path: filelocation}
      }
      return response.json(responseData)
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }
}
