import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Category from "App/Models/Category";
import Logger from "@ioc:Adonis/Core/Logger";
const Help = require("App/Common/Help");
let helper = new Help()
export default class CategoriesCmsController {

  /**
  * @swagger
  * /api-cms/categories:
  *   get:
  *     tags:
  *       - CMS Categories
  *     summary: List Category
  *     security:
  *       - bearerAuth: []
  *     parameters:
  *       - name: order
  *         description: order sort
  *         in: query
  *         required: false
  *       - name: type
  *         description: type category
  *         in: query
  *         required: false
  *       - name: page
  *         description: page category
  *         in: query
  *         required: false
  *       - name: limit
  *         description: limit category
  *         in: query
  *         required: false
  *       - name: status
  *         description: status category
  *         in: query
  *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Category'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async index({ request, response }: HttpContextContract) {
    try {
      let order = request.input("order")
      let type = request.input('type')
      let page = request.input("page", 1)
      let limit = request.input("limit", 20)
      let status = request.input("status")
      let params = {}
      if (order) params["order"] = order
      if (type) params["type"] = type
      if (status) params["status"] = status
      const categories = await Category.query().where(params).orderBy('created_at', 'desc').paginate(page, limit);
      let categoriesData = categories.toJSON().data
      let categoriesMeta = categories.toJSON().meta
      categoriesMeta = helper.formatMeta(categoriesMeta)
      const categoriesJson = categoriesData.map((categoriesData) => categoriesData.serialize())
      Logger.info({ category: categoriesData.length }, "Get list category successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: categoriesJson,
        meta: categoriesMeta
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list category failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api-cms/categories/{id}:
   *   get:
   *     tags:
   *       - CMS Categories
   *     summary: Detail Category
   *     security:
   *       - bearerAuth: []
  *     parameters:
  *       - name: id
  *         description: id category
  *         in: path
  *         required: true
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Category'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async show({ request, response }: HttpContextContract) {
    try {
      let id = request.param("id")
      const category = await Category.find(id);
      if (!category) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Category not found",
          messageKey: "CATEGORY_NOT_FOUND",
          data: null
        })
      }
      Logger.info({ category: 1 }, "Get category successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: category?.toJSON()
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get category failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api-cms/categories/{id}:
   *   put:
   *     tags:
   *       - CMS Categories
   *     summary: Update Category
  *     security:
  *       - bearerAuth: []
  *     parameters:
  *       - name: id
  *         description: id category
  *         in: path
  *         required: true
  *     requestBody:
  *      required: true
  *      content:
  *        application/json:
  *          schema:
  *            properties:
  *              name:
  *                description: name category
  *                type: string
  *                required: true
  *              description:
  *                description: description category
  *                type: string
  *                required: true
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Category'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async update({ request, response }: HttpContextContract) {
    try {
      const category = await Category.find(request.param('id'));
      let responseData = {
        success: true,
        message: 'success',
        data: []
      }
      if (category) {
        category.status = request.input("status", 1);
        category.name = request.input("name");
        category.description = request.input("description");
        category.icon = request.input("icon");
        if (await category.save()) {
          // @ts-ignore
          responseData.data = category.serialize()
          return response.json(responseData)
        } else {
          responseData.success = false
          responseData.message = "Update category fail"
          return response.status(422).send(responseData)
        }
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Update category failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

/**
  * @swagger
   * /api-cms/categories:
   *   post:
   *     tags:
   *       - CMS Categories
   *     summary: Create Category
  *     security:
  *       - bearerAuth: []
  *     requestBody:
  *      required: true
  *      content:
  *        application/json:
  *          schema:
  *            properties:
  *              name:
  *                description: name category
  *                type: string
  *                required: true
  *              description:
  *                description: description in category
  *                type: string
  *                required: false
  *              icon:
  *                description: icon category
  *                type: string
  *                required: false
  *              parentId :
  *                description: parentId category
  *                type: number
  *                required: false
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Category'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async store({ request, response }: HttpContextContract) {
    try {
      const category = new Category();
      category.name = request.input("name");
      category.description = request.input("description");
      category.icon = request.input("icon");
      category.status = request.input("status", 1);
      await category.save();
      const categoryJson = category.serialize()
      Logger.info({ category: category.id }, "Create category successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: categoryJson
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Create category failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api-cms/categories/{id}:
   *   delete:
   *     tags:
   *       - CMS Categories
   *     summary: Delete Category
   *     security:
   *       - bearerAuth: []
   *     parameters:
  *       - name: id
  *         description: id category
  *         in: path
  *         required: true
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async destroy({  response, request, }: HttpContextContract) {
    try {
       let category = await Category.query()
      .where("id", request.param('id'))
      .delete();
      if (!category[0]) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "Category not found",
          messageKey: "CATEGORY_NOT_FOUND",
          data: null
        })
      }
      // return response.redirect("/dashboard");
      return response.status(200).send({
        success: true,
        message: "Success",
        data: null
      })
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Delete category failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }
}
