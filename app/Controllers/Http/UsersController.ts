import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import User from "App/Models/User";

export default class UsersController {

  /**
   * @swagger
   * /api/users/info:
   *   get:
   *     tags:
   *       - User
   *     summary: Info User
   *     security:
   *       - bearerAuth: []
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/UserInfo'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async info({ auth, response }: HttpContextContract) {
    try {
      const userInfo = await User.find(auth.user!.id);
      let responseData = {
        success: true,
        message: 'success',
        data: {...userInfo?.toJSON()}
      }
      return response.json(responseData)
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }
}
