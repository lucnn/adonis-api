import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Category from "App/Models/Category";
import Logger from "@ioc:Adonis/Core/Logger";
import CUtilsConfig from "Config/CUtilsConfig";
const Help = require("App/Common/Help");
let helper = new Help()
export default class CategoriesController {

  /**
  * @swagger
  * /api/categories:
  *   get:
  *     tags:
  *       - Categories
  *     summary: List Category
  *     security:
  *       - bearerAuth: []
  *     parameters:
  *       - name: order
  *         description: order sort
  *         in: query
  *         required: false
  *       - name: type
  *         description: type category
  *         in: query
  *         required: false
  *       - name: page
  *         description: page category
  *         in: query
  *         required: false
  *       - name: limit
  *         description: limit category
  *         in: query
  *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Category'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async index({ request, response }: HttpContextContract) {
    try {
      let order = request.input("order")
      let type = request.input('type')
      let page = request.input("page", 1)
      let limit = request.input("limit", 20)
      let status = CUtilsConfig.status.active // Chỉ lấy bản ghi active
      let params = {}
      if (order) params["order"] = order
      if (type) params["type"] = type
      if (status) params["status"] = status
      const categories = await Category.query().where(params).orderBy('created_at', 'desc').paginate(page, limit);
      let categoriesData = categories.toJSON().data
      let categoriesMeta = categories.toJSON().meta
      categoriesMeta = helper.formatMeta(categoriesMeta)
      const categoriesJson = categoriesData.map((categoriesData) => categoriesData.serialize())
      Logger.info({ category: categoriesData.length }, "Get list category successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: categoriesJson,
        meta: categoriesMeta
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list category failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api/categories/{id}:
   *   get:
   *     tags:
   *       - Categories
   *     summary: Detail Category
   *     security:
   *       - bearerAuth: []
  *     parameters:
  *       - name: id
  *         description: id category
  *         in: path
  *         required: true
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Category'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async show({ request, response }: HttpContextContract) {
    try {
      let id = request.param("id")
      const category = await Category.find(id);
      Logger.info({ category: 1 }, "Get category successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: category?.toJSON()
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get category failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }
}
