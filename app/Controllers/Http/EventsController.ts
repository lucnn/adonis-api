import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Event from "App/Models/Event";
import Logger from "@ioc:Adonis/Core/Logger";
import CUtilsConfig from "Config/CUtilsConfig";
const Help = require("App/Common/Help");
let helper = new Help()
export default class EventsController {
  /**
  * @swagger
  * /api/events:
  *   get:
  *     tags:
  *       - Events
  *     summary: List Event
  *     security:
  *       - bearerAuth: []
  *     parameters:
  *       - name: keyword
  *         description: từ khoá tìm kiếm
  *         in: query
  *         required: false
  *       - name: order
  *         description: order sort
  *         in: query
  *         required: false
  *       - name: type
  *         description: type event
  *         in: query
  *         required: false
  *       - name: position
  *         description: position event
  *         in: query
  *         required: false
   *       - name: limit
   *         description: limit
   *         in: query
   *         required: false
   *       - name: page
   *         description: page
   *         in: query
   *         required: false
   *     responses:
   *       200:
   *         description: Kết quả khi thành công
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/SuccessResponse'
   *              properties:
   *                data:
   *                  schema:
   *                  $ref: '#/definitions/Event'
   *                meta:
   *                  schema:
   *                  $ref: '#/definitions/Meta'
   *       400:
   *         description: Kết quả khi lỗi
   *         content:
   *          application/json:
   *            schema:
   *              $ref: '#/definitions/ErrorResponse'
   */
  public async index({ request, response }: HttpContextContract) {
    try {
      let keyword = request.input("keyword")
      let order = request.input("order")
      let type = request.input('type')
      let position = request.input('position')
      let status = CUtilsConfig.status.active
      let page = request.input('page', 1)
      let limit = request.input('limit', 20)
      let params = {}
      if (order) params["order"] = order
      if (type) params["type"] = type
      if (position) params["position"] = position
      if (status) params["status"] = status
      const eventQuery = Event.query().where(params)
        if(keyword){
          keyword = helper.replaceAll(keyword, '_','\\_')
          keyword = helper.replaceAll(keyword, '%','\\%')
          eventQuery.andWhere('title', 'like', '%'+keyword+'%')
          // eventQuery.andWhere((query) => {
          //   query
          //     .where('title', 'like', '%'+keyword+'%')
          //     .orWhere('content', 'like', '%'+keyword+'%')
          // })
        }
      const events = await eventQuery.orderBy('created_at', 'desc').paginate(page, limit);
      let eventsData = events.toJSON().data
      let eventsMeta = events.toJSON().meta
      eventsMeta = helper.formatMeta(eventsMeta)
      const eventJson = eventsData.map((events) => events.serialize())
      Logger.info({ events: events.length }, "Get list events successfully");
      let responseData = {
        success: true,
        message: 'success',
        data: eventJson,
        meta: eventsMeta
      }
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list events failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }

  /**
   * @swagger
   * /api/events/{id}:
   *   get:
   *     tags:
   *       - Events
   *     summary: Detail Event
   *     security:
   *       - bearerAuth: []
  *     parameters:
  *       - name: id
  *         description: id Event
  *         in: path
  *         required: true
  *     responses:
  *       200:
  *         description: Kết quả khi thành công
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/SuccessResponse'
  *              properties:
  *                data:
  *                  schema:
  *                  $ref: '#/definitions/Event'
  *       400:
  *         description: Kết quả khi lỗi
  *         content:
  *          application/json:
  *            schema:
  *              $ref: '#/definitions/ErrorResponse'
  */
  public async show({ request, response }: HttpContextContract) {
    try {
      let id = request.param("id")
      const event = await Event.find(id);
      let responseData = {
        success: true,
        message: 'success',
        data: event ? event?.toJSON() : null
      }
      Logger.info({ event: 1 }, "Get event successfully");
      return response.json(responseData)
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get event failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error
      })
    }
  }
}
