import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Logger from "@ioc:Adonis/Core/Logger";
import Campaign from "App/Models/Campaign";
import Deal from "App/Models/Deal";
import Order from "App/Models/Order";
import DealValidator from "App/Validators/DealValidator";
import CUtilsConfig from "Config/CUtilsConfig";
import pick from "lodash/pick";
import { DateTime } from "luxon";

export default class DealsController {
  public async index({ request, response, auth }: HttpContextContract) {
    try {
      const user = await auth.authenticate();
      const page = request.input("page", 1);
      const limit = request.input("limit", 20);
      const campaignId = request.input("campaignId");
      const needMe = request.input("needMe");
      const params = { status: CUtilsConfig.dealStatus.active, is_open: 1 };
      if (campaignId) params["campaign_id"] = campaignId;
      const now = DateTime.local().toISO();
      let query = Deal.query()
        .preload("maker")
        .where("expire_at", ">", now)
        .whereNotIn("id", (query) =>
          query
            .from("orders")
            .select("deal_id")
            .where("expire_at", ">", now)
            .where("status", CUtilsConfig.orderStatus.initiated)
            .whereNotNull("deal_id")
        )
        .where(params);
      if (needMe) {
        query = query.whereNot("maker_id", user.id);
      }
      const deals = await query
        .orderBy("created_at", "desc")
        .paginate(page, limit);
      let { data, meta } = deals.toJSON();
      const dealsJson = data.map((data) => {
        const deal = data.serialize();
        return {
          ...deal,
          maker: pick(deal.maker, ["name", "avatar"]),
        };
      });
      Logger.info({ deal: data.length }, "Get list deal successfully");
      return response.json({
        success: true,
        message: "success",
        data: dealsJson,
        meta,
      });
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list deal failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  public async store({ request, response, auth }: HttpContextContract) {
    // use uuid for close deal
    try {
      await request.validate(DealValidator);
      const user = await auth.authenticate();
      const deal = new Deal();
      deal.campaignId = request.input("campaignId");
      const campaign = await Campaign.find(deal.campaignId);
      if (!campaign) {
        return response.status(400).send({
          success: false,
          statusCode: 400,
          message: "campaignId invalid",
        });
      }
      // TODO: check out of stock
      deal.makerId = user.id;
      deal.status = CUtilsConfig.dealStatus.pending;
      deal.isOpen = request.input("isOpen");
      await deal.save();
      return response.json({
        success: true,
        message: "success",
        data: deal.serialize(),
      });
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Create deal failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  public async show({ request, response }: HttpContextContract) {
    try {
      const id = request.param("id");
      const deal = await Deal.query()
        .where("expire_at", ">", DateTime.local().toISO())
        .where({ id, status: CUtilsConfig.dealStatus.active })
        .first();
      const responseData = {
        success: true,
        message: "success",
        data: deal ? deal.toJSON() : null,
      };
      Logger.info({ deal: 1 }, "Get deal successfully");
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get deal failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }

  public async checkCloseDeal({
    request,
    response,
    auth,
  }: HttpContextContract) {
    try {
      const user = await auth.authenticate();
      const id = request.param("id");
      const deal = await Deal.query()
        .where("expire_at", ">", DateTime.local().toISO())
        .where({ id, status: CUtilsConfig.dealStatus.active, is_open: 0 })
        .first();
      let valid = false;
      if (deal) {
        const order = await Order.query()
          .where({
            deal_id: id,
            status: CUtilsConfig.orderStatus.pending,
          })
          .first();
        valid = !!order && order.userId !== user.id;
      }
      const responseData = {
        success: true,
        message: "success",
        data: { valid, campaignId: deal?.campaignId },
      };
      return response.json(responseData);
    } catch (error) {
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }
}
