import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Logger from "@ioc:Adonis/Core/Logger";
import PointHistory from "App/Models/PointHistory";
const Help = require("App/Common/Help");
let helper = new Help();

export default class PointHistoriesController {
  public async index({ request, response, auth }: HttpContextContract) {
    try {
      let page = request.input("page", 1);
      let limit = request.input("limit", 20);
      const user = await auth.authenticate();
      let params = { user_id: user.id };
      console.log(params);

      const pointHistoryQuery = PointHistory.query().where(params);
      const pointHistories = await pointHistoryQuery
        .orderBy("created_at", "desc")
        .paginate(page, limit);
      let pointHistoriesData = pointHistories.toJSON().data;
      let pointHistoriesMeta = pointHistories.toJSON().meta;
      pointHistoriesMeta = helper.formatMeta(pointHistoriesMeta);
      const pointHistoryJson = pointHistoriesData.map((pointHistories) =>
        pointHistories.serialize()
      );
      Logger.info(
        { pointHistories: pointHistories.length },
        "Get list point histories successfully"
      );
      let responseData = {
        success: true,
        message: "success",
        data: pointHistoryJson,
        meta: pointHistoriesMeta,
      };
      return response.json(responseData);
    } catch (error) {
      Logger.error({ err: new Error(error) }, "Get list point history failed");
      return response.status(400).send({
        success: false,
        statusCode: 400,
        message: error.message,
        errors: error,
      });
    }
  }
}
