import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
const camelcaseKeys = require('camelcase-keys');

export default class MyResponse {
  public async handle ({ response }: HttpContextContract, next: () => Promise<void>) {
    await next()
    const existingBody = response.lazyBody[0]
    if (!existingBody || existingBody.constructor !== Object) {
      return
    }
    if(existingBody.openapi){ // gen doc swagger
      response.send(existingBody)
    }else{
      if (existingBody.message === 'E_VALIDATION_FAILURE: Validation Exception') {
        existingBody.messageKey = 'E_VALIDATION_FAILURE'
        if(existingBody.errors.messages){
          if(existingBody.errors.messages.errors){
            existingBody.errors = existingBody.errors.messages.errors
          }else{
            existingBody.errors = [existingBody.errors.messages]
          }
          console.log(existingBody.errors)
          if(existingBody.errors[0]){
            existingBody.message = existingBody.errors[0].message
            existingBody.messageKey = existingBody.errors[0].rule+'_'+existingBody.errors[0].field
            existingBody.messageKey = existingBody.messageKey.toUpperCase()
          }
        }
      }else if(existingBody.errors && existingBody.errors.guard !== undefined){
        // console.log(existingBody.errors.guard)
        // console.log("==========") //E_INVALID_AUTH_UID: User not found
        // console.log("==========") //E_INVALID_AUTH_PASSWORD: Password mis-match
        if(existingBody.message.includes("INVALID_AUTH_UID") == true){
          existingBody.messageKey = 'INVALID_AUTH_UID'
        }else if(existingBody.message.includes("INVALID_AUTH_PASSWORD") == true){
          existingBody.messageKey = 'INVALID_AUTH_PASSWORD'
        }
      }else if(existingBody.errors && existingBody.errors.code !== undefined){
        existingBody.messageKey = existingBody.errors.code
      }
      response.send(camelcaseKeys(existingBody, {deep: true, stopPaths: ['a_c.c_e']}))
    }
  }
}
