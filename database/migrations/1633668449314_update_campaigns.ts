import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UpdateCampaigns extends BaseSchema {
  protected tableName = 'campaigns'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('free_shipping').defaultTo(1).index().comment('0 - ko miễn phí, 1 - miễn phí')
      table.double('shipping_fee').defaultTo(0).comment('nếu free_shipping = 1 thì set phí')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
