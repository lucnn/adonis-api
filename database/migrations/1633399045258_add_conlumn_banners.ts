import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AddConlumnBanners extends BaseSchema {
  protected tableName = 'banners'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('event_id').index()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
