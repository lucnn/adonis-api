import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AddColumnBanners extends BaseSchema {
  protected tableName = 'banners'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('duration_start').index()
      table.integer('duration_end').index()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
