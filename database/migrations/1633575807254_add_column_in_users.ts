import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AddColumnInUsers extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.string('avatar', 255)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
