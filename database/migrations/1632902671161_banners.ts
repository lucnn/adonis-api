import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Banners extends BaseSchema {
  protected tableName = 'banners'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name')
      table.string('path')
      table.integer('order').index()
      table.integer('type').index().defaultTo(0).comment('0 - trang home')
      table.integer('position').index().defaultTo(0).comment('0 - trang home top')
      table.integer('status').index().defaultTo(0).comment('0 - not active, 1 - active')
      table.timestamps()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
