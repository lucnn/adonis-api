import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ModifyVerifyCodes extends BaseSchema {
  protected tableName = 'verify_codes'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('lockTime')
      table.integer('lockCount')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
