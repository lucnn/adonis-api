import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Deals extends BaseSchema {
  protected tableName = "deals";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.integer("campaign_id").notNullable().index();
      table.integer("maker_id").notNullable().index();
      table.timestamps();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
