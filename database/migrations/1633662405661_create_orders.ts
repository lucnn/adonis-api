import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CreateOrders extends BaseSchema {
  protected tableName = 'orders'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('user_id').nullable().index()
      table.integer('campaign_id').nullable().index()
      table.integer('user_address_id').nullable().index()
      table.json('options').comment('thông tin option của campaign')
      table.integer('status').defaultTo(0).index().comment('0- chờ thanh toán')
      table.integer('quantity')
      table.double('total_price')
      table.double('shipping_fee')
      table.double('grand_total')
      table.integer('payment_method').defaultTo(0).index().comment('0 - vnpay')
      table.timestamps()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
