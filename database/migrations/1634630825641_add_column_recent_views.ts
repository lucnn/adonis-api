import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AddColumnRecentViews extends BaseSchema {
  protected tableName = 'recent_views'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('view_count')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
