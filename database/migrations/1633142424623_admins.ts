import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Admins extends BaseSchema {
  protected tableName = 'admins'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name', 255).notNullable()
      table.string('email', 255)
      table.string('password', 180).notNullable()
      table.string('remember_me_token').nullable()
      table.integer('type').defaultTo(0).index().comment('0 user, 1 - admin cms')
      table.string('username', 255).notNullable().unique()
      table.string('phone', 255).notNullable().unique()
      table.integer('very_phone').defaultTo(0).index().comment('0 not very, 1 - very')
      table.dateTime('birthday')
      table.timestamps()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
