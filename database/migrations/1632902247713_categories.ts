import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Categories extends BaseSchema {
  protected tableName = 'categories'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name').notNullable()
      table.text('description')
      table.string('icon')
      table.integer('parent_id').index()
      table.integer('order').index()
      table.integer('type').index().defaultTo(0).comment('0 - san pham')
      table.integer('status').index().defaultTo(0).comment('0 - not active, 1 - active')
      table.timestamps()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
