import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class PointHistories extends BaseSchema {
  protected tableName = "point_histories";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.integer("user_id").notNullable();
      table.string("type", 255);
      table.bigInteger("value");
      table.timestamps();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
