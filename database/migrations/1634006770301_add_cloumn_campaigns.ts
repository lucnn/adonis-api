import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AddCloumnCampaigns extends BaseSchema {
  protected tableName = 'campaigns'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('participation_rate').defaultTo(0)
      table.integer('accomplishment_rate').defaultTo(0)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
