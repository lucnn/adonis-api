import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AddColumnVeriCodes extends BaseSchema {
  protected tableName = 'verify_codes'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.string('token_very', 255)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
