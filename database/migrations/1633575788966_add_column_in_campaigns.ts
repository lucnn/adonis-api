import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AddColumnInCampaigns extends BaseSchema {
  protected tableName = 'campaigns'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.json('benefits_tag')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
