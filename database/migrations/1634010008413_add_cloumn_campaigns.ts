import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AddCloumnCampaigns extends BaseSchema {
  protected tableName = 'campaigns'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('amount_of_sales').defaultTo(0)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
