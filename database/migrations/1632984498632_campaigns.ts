import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Campaigns extends BaseSchema {
  protected tableName = 'campaigns'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('name')
      table.string('brand')
      table.string('thumbnail')
      table.text('content')
      table.json('images')
      table.json('product_options')
      table.json('color_options')
      table.integer('user_id').index()
      table.integer('view_count')
      table.integer('status').index()
      table.integer('category_id').index()
      table.integer('deal_duration_start').index().comment('lưu dạng timestamp')
      table.integer('deal_duration_end').index().comment('lưu dạng timestamp')
      table.double('original_price')
      table.double('discount_price')
      table.double('deal_price')
      table.integer('mega_deal').index().defaultTo(0).comment('0 not deal, 1 deal')
      table.double('extra_discount_rate')
      table.double('refund_points')
      table.double('target_number_people')
      table.integer('mega_deal_duration_start').comment('lưu dạng timestamp')
      table.integer('mega_deal_duration_end').comment('lưu dạng timestamp')
      table.timestamps()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
