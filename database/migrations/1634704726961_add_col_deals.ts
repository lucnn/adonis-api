import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class AddColDeals extends BaseSchema {
  protected tableName = "deals";

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer("is_open");
      table.integer("status");
      table.dateTime("expire_at");
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
