import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AddColumnVerifies extends BaseSchema {
  protected tableName = 'verify_codes'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('expired_at')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
