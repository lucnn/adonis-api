import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AdminIdCampaigns extends BaseSchema {
  protected tableName = 'campaigns'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('admin_id').index()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
