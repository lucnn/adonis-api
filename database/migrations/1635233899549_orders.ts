import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Orders extends BaseSchema {
  protected tableName = "orders";

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.string("shipping_date", 255);
      table.string("tracking_number", 255);
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
