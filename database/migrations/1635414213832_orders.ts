import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Orders extends BaseSchema {
  protected tableName = "orders";

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.bigInteger("point_discount");
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
