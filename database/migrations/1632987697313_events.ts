import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Events extends BaseSchema {
  protected tableName = 'events'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('title').notNullable()
      table.string('image')
      table.text('content')
      table.integer('duration_start').index()
      table.integer('duration_end').index()
      table.integer('status').index().defaultTo(0).comment('0 - waiting, 1 - in progress, 2 - closed')
      table.timestamps()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
