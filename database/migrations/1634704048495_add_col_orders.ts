import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class AddColOrders extends BaseSchema {
  protected tableName = "orders";

  public async up() {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer("deal_id").index();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
