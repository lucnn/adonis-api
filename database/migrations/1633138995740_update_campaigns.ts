import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class UpdateCampaigns extends BaseSchema {
  protected tableName = 'campaigns'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.renameColumn('product_options', 'options')
      table.dropColumn('color_options')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
