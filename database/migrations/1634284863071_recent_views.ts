import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class RecentViews extends BaseSchema {
  protected tableName = 'recent_views'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('user_id').notNullable().index()
      table.integer('campaign_id').notNullable().index()
      table.timestamps()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
