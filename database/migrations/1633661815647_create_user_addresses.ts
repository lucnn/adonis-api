import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CreateUserAddresses extends BaseSchema {
  protected tableName = 'user_addresses'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('user_id').nullable().index()
      table.string('contact_name', 255)
      table.string('phone', 255)
      table.string('city', 255)
      table.string('district', 255)
      table.string('wards', 255)
      table.string('address', 255)
      table.integer('set_default').index()
      table.timestamps()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
