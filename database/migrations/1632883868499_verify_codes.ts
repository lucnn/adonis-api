import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class VerifyCodes extends BaseSchema {
  protected tableName = 'verify_codes'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('type').index().defaultTo(0).comment('0 - verify phone')
      table.integer('status').index().defaultTo(0).comment('0 - not verify, 1 - verify ok')
      table.string('payload').comment('là số điện thoại nếu type = 0')
      table.string('code').comment('mã verify')
      table.timestamps()
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
