import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class AlterUsersTables extends BaseSchema {
  protected tableName = 'users'

  public async up () {
    this.schema.alterTable(this.tableName, (table) => {
      table.integer('type').defaultTo(0).index().comment('0 user, 1 - admin cms')
      table.string('username', 255).notNullable().unique()
      table.string('phone', 255).notNullable().unique()
      table.integer('very_phone').defaultTo(0).index().comment('0 not very, 1 - very')
      table.dateTime('birthday')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
