import BaseSeeder from "@ioc:Adonis/Lucid/Seeder";
import Category from "App/Models/Category";

export default class CategorySeeder extends BaseSeeder {
  public async run() {
    await Category.createMany([
      {
        name: "Beauty",
        icon: "/images/beauty.svg",
        order: 1,
        status: 1,
      },
      {
        name: "Fashion",
        icon: "/images/fashion.svg",
        order: 2,
        status: 1,
      },
      {
        name: "Digital",
        icon: "/images/digital.svg",
        order: 3,
        status: 1,
      },
      {
        name: "Home & Life",
        icon: "/images/home-life.svg",
        order: 4,
        status: 1,
      },
      {
        name: "Furniture",
        icon: "/images/furniture.svg",
        order: 5,
        status: 1,
      },
      {
        name: "Food",
        icon: "/images/food.svg",
        order: 6,
        status: 1,
      },
      {
        name: "Accessories",
        icon: "/images/accessories.svg",
        order: 7,
        status: 1,
      },
      {
        name: "Sports",
        icon: "/images/sports.svg",
        order: 8,
        status: 1,
      },
      {
        name: "Kids & Babies",
        icon: "/images/kids-babies.svg",
        order: 9,
        status: 1,
      },
      {
        name: "Pet",
        icon: "/images/pet.svg",
        order: 10,
        status: 1,
      },
    ]);
  }
}
