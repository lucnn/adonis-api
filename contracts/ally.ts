import { GoogleDriverConfig, GoogleDriverContract, TwitterDriverConfig, TwitterDriverContract, FacebookDriverConfig, FacebookDriverContract } from '@ioc:Adonis/Addons/Ally';
declare module '@ioc:Adonis/Addons/Ally' {
    interface SocialProviders {
        google: {
            config: GoogleDriverConfig
            implementation: GoogleDriverContract
        }
        facebook: {
            config: FacebookDriverConfig
            implementation: FacebookDriverContract
        }
    }
}