/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes/index.ts` as follows
|
| import './cart'
| import './customer'
|
*/

import Route from "@ioc:Adonis/Core/Route";

Route.get("/", "ForumsController.test");

//Login social
Route.get('facebook/redirect', 'LoginSocialController.redirectFacebook');
Route.get('facebook/callback', 'LoginSocialController.callbackFacebook');
Route.get('zalo/redirect', 'LoginSocialController.redirectZalo');
Route.get('zalo/callback', 'LoginSocialController.callbackZalo');
Route.get('google/redirect', 'LoginSocialController.redirectGoogle');
Route.get('google/callback', 'LoginSocialController.callbackGoogle');

Route.group(() => {
  Route.post("register", "AuthController.register");
  Route.post("login", "AuthController.login");
  Route.post("verify", "VeryCodeController.verify");
  Route.post("request-code", "VeryCodeController.sendOtpToPhone");
  Route.post("forgot-password", "AuthController.forgotPassword");

  //Login social
  Route.get("facebook/redirect", "LoginSocialController.redirectFacebook");
  Route.get("facebook/callback", "LoginSocialController.callbackFacebook");
  Route.get("zalo/redirect", "LoginSocialController.redirectZalo");
  Route.get("zalo/callback", "LoginSocialController.callbackZalo");
  Route.get("google/redirect", "LoginSocialController.redirectGoogle");
  Route.get("google/callback", "LoginSocialController.callbackGoogle");
  Route.group(() => {
    Route.get("categories", "CategoriesController.index");
    Route.get("categories/:id", "CategoriesController.show");
    Route.get("banners", "BannersController.index");
    Route.get("banners/:id", "BannersController.show");
    Route.get("events", "EventsController.index");
    Route.get("events/:id", "EventsController.show");
    Route.get("campaigns/favorite-list", "CampaignsController.favoriteList");
    Route.get("campaigns/recent-views", "CampaignsController.recentViews");
    Route.get("campaigns", "CampaignsController.index");
    Route.get(
      "/campaigns/suggested-keywords",
      "CampaignsController.getSuggestedKeywords"
    );
    Route.get(
      "campaigns/search-homepage",
      "CampaignsController.searchHomepage"
    );
    Route.get("campaigns/:id", "CampaignsController.show");
    Route.post("campaigns/favorite", "CampaignsController.favorite");
    Route.get("users/info", "UsersController.info");

    Route.post("files/upload", "FilesController.upload");
    Route.resource("user-address", "UserAddressController").apiOnly();
    Route.put(
      "update-address-default/:id",
      "UserAddressController.updateAddressDefault"
    );
    Route.get("deals", "DealsController.index");
    Route.post("deals", "DealsController.store");
    Route.get("deals/:id", "DealsController.show");
    Route.get("deals/check-close-deal/:id", "DealsController.checkCloseDeal");
    //Order
    Route.get("orders", "OrderController.index");
    Route.get("orders/:id", "OrderController.show");
    Route.post("orders/:id/cancel", "OrderController.cancel");
    Route.post("orders/:id/complete", "OrderController.complete");
    Route.post("orders/:id/refund", "OrderController.refund");
    Route.post("orders", "OrderController.store");
    Route.get("payments", "PaymentsController.index");
    Route.get(
      "payments/vnpay-create-url/:orderId",
      "PaymentsController.vnPayCreateUrl"
    );
    Route.get("point-histories", "PointHistoriesController.index");

    //update info login social
    Route.post('social/register', 'LoginSocialController.register');
  }).middleware("auth:api");

  Route.get("payments/vnpay-ipn", "PaymentsController.vnPayIPN");
  Route.get("payments/vnpay-return", "PaymentsController.vnPayReturn");
}).prefix("api");

Route.group(() => {
  Route.post("register", "AuthCmsController.register");
  Route.post("login", "AuthCmsController.login");

  Route.group(() => {
    Route.resource("categories", "CategoriesCmsController").apiOnly();
    Route.resource("banners", "BannersCmsController").apiOnly();
    Route.resource("events", "EventsCmsController").apiOnly();
    Route.resource("campaigns", "CampaignsCmsController").apiOnly();
    Route.resource("orders", "OrdersCmsController").apiOnly();
    Route.post("campaigns/favorite", "CampaignsCmsController.favorite");
    Route.get("users/info", "UsersCmsController.info");

    Route.post("files/upload", "FilesCmsController.upload");
  }).middleware("auth:apiAdmin");
})
  .namespace("App/Controllers/Http/Cms")
  .prefix("api-cms");
