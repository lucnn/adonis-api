# adonisjs-api
https://adonisjs.com/
https://www.section.io/engineering-education/build-a-restful-api-with-adonisjs/
###
AdonisJS is a Node.js framework, and hence it requires Node.js to be installed on your computer. 
To be precise, we need at least the latest release of Node.js 14, along with npm >= 6.0.0.
# create db
```
create database uniz character set UTF8 collate utf8_bin;
```
# run app => modify config in .env
```
npm install

cp .env.example .env

node ace migration:run

node ace serve --watch
```
#Doc swagger
http://localhost:3001/docs

#Create Scheduler & Run

```
node ace make:task MyTaskName
node ace scheduler:run
```
```
public static get schedule() {
// *    *    *    *    *    *
// ┬    ┬    ┬    ┬    ┬    ┬
// │    │    │    │    │    │
// │    │    │    │    │    └ day of week (0 - 7) (0 or 7 is Sun)
// │    │    │    │    └───── month (1 - 12)
// │    │    │    └────────── day of month (1 - 31)
// │    │    └─────────────── hour (0 - 23)
// │    └──────────────────── minute (0 - 59)
// └───────────────────────── second (0 - 59, OPTIONAL)
  return '0 * * * * *'
}
```

#Build BE
```
node ace build --production
cp .env build/
ln -s /var/www/backend/public /var/www/backend/build/
cd build/
node server.js
```
