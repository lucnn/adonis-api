import Env from '@ioc:Adonis/Core/Env'
const zaloConfig = {
    clientId: Env.get('ZALO_CLIENT_ID'),
    clientSecret: Env.get('ZALO_CLIENT_SECRET'),
    callbackUrl: Env.get('DOMAIN') + '/zalo/callback',
    zaloUrl: Env.get('ZALO_URL_API'),
    zaloOauth: Env.get('ZALO_OAUTH'),
    zaloApiGraphMe: Env.get('ZALO_API_GRAPH_ME')
}

export default zaloConfig