import Env from '@ioc:Adonis/Core/Env'
const CUtilsConfig = {
  token: {
    expiresIn: "10 days",
  },
  status: {
    warning: 0,
    active: 1,
    cancel: 2,
  },
  dealStatus: {
    pending: 0,
    active: 1,
    matched: 2,
  },
  orderStatus: {
    unmatched: -1,
    paymentFailed: 0,
    initiated: 1,
    pending: 2,
    cancelled: 3,
    matched: 4,
    packing: 5,
    onDelivery: 6,
    completed: 7,
    refundProcess: 8,
    refunded: 9,
  },
  pointHistoryType: {
    refundPoint: "refund_point",
    usageForDiscount: "usage_for_discount",
    openDealIncentive: "open_deal_incentive",
    refundProcess: "refund_process",
    returnForCancellation: "point_return_for_cancellation"
  },
  megaDeal: {
    warning: 0,
    active: 1,
  },
  userType: {
    default: 0,
    facebook: 1,
    google: 2,
    zalo: 3
  },
  redirectURI: Env.get('REDIRECT_URI'),
  statusLoginSocial: {
    firstLogin: 0,
    isLogin: 1
  },
  verifyCodeType: {
    registerDefault: 0,
    forgotPassword: 1,
    registerSocial: 2
  }
};

export default CUtilsConfig;
