import Env from '@ioc:Adonis/Core/Env'
import { AllyConfig } from '@ioc:Adonis/Addons/Ally'

const allyConfig: AllyConfig = {
    google: {
        driver: 'google',
        clientId: Env.get('GOOGLE_CLIENT_ID'),
        clientSecret: Env.get('GOOGLE_CLIENT_SECRET'),
        callbackUrl: Env.get('DOMAIN') + '/google/callback',
    },
    facebook: {
        driver: 'facebook',
        clientId: Env.get('FACEBOOK_CLIENT_ID'),
        clientSecret: Env.get('FACEBOOK_CLIENT_SECRET'),
        callbackUrl: Env.get('DOMAIN') + '/facebook/callback',
    },
}

export default allyConfig